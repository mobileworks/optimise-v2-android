package uk.co.symec.optimise2.vm

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.PowerManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.optimise2.domain.model.TestResultModel
import uk.co.symec.optimise2.domain.usecase.GetBatteryInfoUseCase
import uk.co.symec.optimise2.domain.usecase.GetLastTestResultUseCase
import uk.co.symec.optimise2.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
public class PermissionsViewModel @Inject constructor(
    val getBatteryInfoUseCase: GetBatteryInfoUseCase,
    val getLastTestResultUseCase: GetLastTestResultUseCase
) : BaseViewModel<TestResultModel>(),
    LifecycleObserver {

    var locationState = MutableLiveData(false)
    var phoneState = MutableLiveData(false)
    var powerState = MutableLiveData(false)
    var backgroundState = MutableLiveData(false)

    fun checkPermissions(context: Context) {
        locationState.value = (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
                + ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
                == PackageManager.PERMISSION_GRANTED)

        phoneState.value = (
                ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_PHONE_STATE
                )
                        == PackageManager.PERMISSION_GRANTED
                )

        (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).let { activityManager ->
            backgroundState.value = !activityManager.isBackgroundRestricted
        }

        (context.getSystemService(Context.POWER_SERVICE) as PowerManager).let { powerManager ->
            powerState.value = powerManager.isIgnoringBatteryOptimizations(context.packageName)
        }
    }
}