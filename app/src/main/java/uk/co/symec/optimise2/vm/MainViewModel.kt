package uk.co.symec.optimise2.vm

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.PowerManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.optimise2.domain.model.*
import uk.co.symec.optimise2.domain.usecase.GetBatteryHealthUseCase
import uk.co.symec.optimise2.domain.usecase.GetBatteryInfoUseCase
import uk.co.symec.optimise2.domain.usecase.GetDailyScreenOnTimeUseCase
import uk.co.symec.optimise2.domain.usecase.GetLastTestResultUseCase
import uk.co.symec.optimise2.ui.base.*
import uk.co.symec.optimise2.ui.base.Success
import uk.co.symec.optimise2.utils.GPS_WORKER
import uk.co.symec.optimise2.utils.TAG_GPS_WORKER
import uk.co.symec.optimise2.utils.TAG_TEST_WORKER
import uk.co.symec.optimise2.utils.TEST_WORKER
import uk.co.symec.optimise2.worker.GpsWorker
import uk.co.symec.optimise2.worker.TestWorker
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
public class MainViewModel @Inject constructor(
    val getBatteryInfoUseCase: GetBatteryInfoUseCase,
    val getLastTestResultUseCase: GetLastTestResultUseCase,
    val getBatteryHealthUseCase: GetBatteryHealthUseCase,
    val getDailyScreenOnTimeUseCase: GetDailyScreenOnTimeUseCase
) : BaseViewModel<TestResultModel>(),
    LifecycleObserver {

    protected val _resultState = MutableLiveData<ViewState<TestResultModel>>()
    val resultState: LiveData<ViewState<TestResultModel>>
        get() = _resultState

    protected val _batteryState = MutableLiveData<ViewState<BatteryInfoModel>>()
    val batteryState: LiveData<ViewState<BatteryInfoModel>>
        get() = _batteryState

    protected val _improvementState = MutableLiveData<ImprovementState<ImprovementModel>>()
    val improvementState: LiveData<ImprovementState<ImprovementModel>>
        get() = _improvementState

    var wifiValue = MutableLiveData<WifiStatsModel>()
    var gsmValue = MutableLiveData<MobileStatsModel>()
    var ramValue = MutableLiveData<MemoryStatsModel>()
    var storageValue = MutableLiveData<StorageStatsModel>()

    // New instance variable for the WorkInfo
//    internal val outputWorkInfosConnection: LiveData<List<WorkInfo>> =
//        workManager.getWorkInfosByTagLiveData(
//            TAG_TEST_WORKER
//        )

    fun getLastResult() = executeUseCase {
        getLastTestResultUseCase().onSuccess {
            _resultState.value = Success(it)
        }
    }

    fun getBatteryInfo() = executeUseCase {
        getBatteryInfoUseCase().onSuccess {
            _batteryState.value = Success(it)
        }
    }

    fun getBatteryHealth() = executeUseCase {
        getBatteryHealthUseCase().onSuccess {
            println(it)
            //_batteryState.value = Success(it)
        }
    }

    fun getScreenOnTime() = executeUseCase {
        val date = Calendar.getInstance()
        date.set(Calendar.DAY_OF_MONTH, 20)
        getDailyScreenOnTimeUseCase(date.time).onSuccess {
            println(it)
            //_batteryState.value = Success(it)
        }
    }

    fun updateWifiModel(wifiModel: WifiStatsModel) {
        wifiValue.value = wifiModel
        calculate()
    }

    fun updateMobileModel(model: MobileStatsModel) {
        gsmValue.value = model
        calculate()
    }

    private fun calculate() {
        var criticalCount = 0
        var warningCount = 0

        var wifiStatus = 0
        var gsmStatus = 0
        var ramStatus = 0
        var storageStatus = 0

        wifiValue.value?.let {
            if (it.isAvailable) {
                when (it.status) {
                    2 -> criticalCount++
                    1 -> warningCount++
                    else -> 0
                }
                wifiStatus = it.status
            } else {
                gsmValue.value?.let {
                    if (it.isAvailable) {
                        when (it.status) {
                            2 -> criticalCount++
                            1 -> warningCount++
                            else -> 0
                        }
                    }
                    gsmStatus = it.status
                }
            }
        }

        ramValue.value?.let {
            when (it.status) {
                2 -> criticalCount++
                1 -> warningCount++
                else -> 0
            }
            ramStatus = it.status
        }

        storageValue.value?.let {
            when (it.status) {
                2 -> criticalCount++
                1 -> warningCount++
                else -> 0
            }
            storageStatus = it.status
        }

        if (criticalCount > 0 && warningCount > 0) {
            _improvementState.value = ShowAll(ImprovementModel(warningCount, criticalCount, storageStatus, ramStatus, gsmStatus, wifiStatus))
        } else {
            when {
                criticalCount > 0 -> {
                    _improvementState.value =
                        ShowCritical(ImprovementModel(warningCount, criticalCount, storageStatus, ramStatus, gsmStatus, wifiStatus))
                }
                warningCount > 0 -> {
                    _improvementState.value = ShowWarning(ImprovementModel(warningCount, criticalCount, storageStatus, ramStatus, gsmStatus, wifiStatus))
                }
                else -> {
                    _improvementState.value = AllOk(ImprovementModel(warningCount, criticalCount, storageStatus, ramStatus, gsmStatus, wifiStatus))
                }
            }
        }
    }

    fun updateRamMemory(model: MemoryStatsModel) {
        ramValue.value = model
        calculate()
    }

    fun updateStorage(model: StorageStatsModel) {
        storageValue.value = model
        calculate()
    }

    fun startWorker(intervalInSec: Long) {
        if (intervalInSec > 900) {
            val work = PeriodicWorkRequestBuilder<GpsWorker>(intervalInSec, TimeUnit.SECONDS)
                .addTag(TAG_GPS_WORKER)
                .build()
            workManager.enqueueUniquePeriodicWork(
                GPS_WORKER,
                ExistingPeriodicWorkPolicy.KEEP,
                work
            )
        } else {
            val tasks = 900 / intervalInSec
            for (taskNo in 0 until tasks) {
                val work = PeriodicWorkRequestBuilder<GpsWorker>(intervalInSec, TimeUnit.SECONDS)
                    .addTag(TAG_GPS_WORKER)
                    .setInitialDelay(taskNo * intervalInSec, TimeUnit.SECONDS)
                    .build()
                workManager.enqueue(work)
            }
        }
    }

    fun startWorkerTest(intervalInSec: Long) {
        if (intervalInSec > 900) {
            val work = PeriodicWorkRequestBuilder<TestWorker>(intervalInSec, TimeUnit.SECONDS)
                .addTag(TAG_TEST_WORKER)
                .build()
            workManager.enqueueUniquePeriodicWork(
                TEST_WORKER,
                ExistingPeriodicWorkPolicy.KEEP,
                work
            )
        } else {
            val tasks = 900 / intervalInSec
            for (taskNo in 0 until tasks) {
                val work = PeriodicWorkRequestBuilder<TestWorker>(intervalInSec, TimeUnit.SECONDS)
                    .addTag(TAG_TEST_WORKER)
                    .setInitialDelay(taskNo * intervalInSec, TimeUnit.SECONDS)
                    .build()
                workManager.enqueue(work)
            }
        }
    }

    fun checkPermissions(context: Context): Boolean {
        val permissions = (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
                + ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
                + ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED
                )
        var background = false
        (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).let { activityManager ->
            background = !activityManager.isBackgroundRestricted
        }

        var power = false
        (context.getSystemService(Context.POWER_SERVICE) as PowerManager).let { powerManager ->
            power = powerManager.isIgnoringBatteryOptimizations(context.packageName)
        }

        return permissions && background && power
    }
}