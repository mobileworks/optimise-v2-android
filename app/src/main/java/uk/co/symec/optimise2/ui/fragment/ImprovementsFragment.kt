package uk.co.symec.optimise2.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uk.co.symec.optimise2.R
import uk.co.symec.optimise2.databinding.ImprovementsFragmentBinding
import uk.co.symec.optimise2.domain.model.ImprovementItemModel
import uk.co.symec.optimise2.domain.model.ImprovementModel
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.ui.adapter.ImprovementsListAdapter
import uk.co.symec.optimise2.vm.MainViewModel


@AndroidEntryPoint
class ImprovementsFragment : BottomSheetDialogFragment() {

    companion object {
        fun newInstance(model: ImprovementModel) = ImprovementsFragment().apply {
            arguments = Bundle().apply {
                putInt("noOfCritical", model.noOfCritical)
                putInt("noOfWarning", model.noOfWarning)
                putInt("ramStatus", model.ramStatus)
                putInt("storageStatus", model.storageStatus)
                putInt("wifiStatus", model.wifiStatus)
                putInt("gsmStatus", model.mobileStatus)
            }
        }
    }

    private val parentViewModel: MainViewModel by activityViewModels()
    private var _binding: ImprovementsFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var criticalAdapter: ImprovementsListAdapter
    lateinit var alertAdapter: ImprovementsListAdapter

    var noOfCritical: Int = 0
    var noOfWarning: Int = 0

    var ramStatus: Int = 0
    var storageStatus: Int = 0
    var gsmStatus: Int = 0
    var wifiStatus: Int = 0

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        noOfCritical = arguments?.getInt("noOfCritical") ?: 0
        noOfWarning = arguments?.getInt("noOfWarning") ?: 0

        ramStatus = arguments?.getInt("ramStatus") ?: 0
        storageStatus = arguments?.getInt("storageStatus") ?: 0
        wifiStatus = arguments?.getInt("wifiStatus") ?: 0
        gsmStatus = arguments?.getInt("gsmStatus") ?: 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        _binding = ImprovementsFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        //binding.viewModel = viewModel
        binding.tvCriticalTitle.text = getString(R.string.high_impact, noOfCritical)
        binding.tvAlertTitle.text = getString(R.string.medium_impact, noOfWarning)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvAlert.layoutManager = LinearLayoutManager(activity)
        binding.rvAlert.setHasFixedSize(true)
        alertAdapter = ImprovementsListAdapter()

        GlobalScope.launch(context = Dispatchers.Main) {
            binding.rvAlert.adapter = alertAdapter
            val testAlertList = mutableListOf<ImprovementItemModel>()
            testAlertList.addAll(generateList(ImprovementType.ALERT))
            alertAdapter.setImprovementsList(testAlertList)
        }

        binding.rvCritical.layoutManager = LinearLayoutManager(activity)
        binding.rvCritical.setHasFixedSize(true)
        criticalAdapter = ImprovementsListAdapter()

        GlobalScope.launch(context = Dispatchers.Main) {
            binding.rvCritical.adapter = criticalAdapter
            val testCriticalList = mutableListOf<ImprovementItemModel>()
            testCriticalList.addAll(generateList(ImprovementType.CRITICAL))
            criticalAdapter.setImprovementsList(testCriticalList)
        }

//        dialog?.setOnShowListener { dialog ->
//            val d = dialog as BottomSheetDialog
//            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
//            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//            bottomSheetBehavior.isDraggable = true
//        }
    }

    private fun generateList(type: ImprovementType): List<ImprovementItemModel> {
        val status = when (type) {
            ImprovementType.CRITICAL -> 2
            ImprovementType.ALERT -> 1
        }

        val list = mutableListOf<ImprovementItemModel>()
        if (ramStatus == status) {
            list.add(ImprovementItemModel(type, "To many applications working in the background", "Close applications which you’re not planning to use soon. It will restore important for device performance RAM.", "performance"))
        }
        if (storageStatus == status) {
            list.add(ImprovementItemModel(type, "Free disk space is running out", "Amount of data stored on the device getting closer to 100%. This has high impact for device performance.  Clear useless data freeing the necessary memory for smooth operation", "performance"))
        }
        if (gsmStatus == status) {
            list.add(ImprovementItemModel(type, "Use GSM with strong signal", "If your signal is weak, it’s worth to consider switch WIFI off. You will save battery consumption and internet connection will get stable", "battery"))
        }
        if (wifiStatus == status) {
            list.add(ImprovementItemModel(type, "Use WiFi with strong signal", "If your signal is weak, it’s worth to consider switch WIFI off. You will save battery consumption and internet connection will get stable", "battery"))
        }
        return list
    }
}
