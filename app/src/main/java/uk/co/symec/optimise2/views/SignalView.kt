package uk.co.symec.optimise2.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat
import uk.co.symec.optimise2.R


class SignalView : View {

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        val a = context!!.theme.obtainStyledAttributes(attrs, R.styleable.StatsGauge, 0, 0)
        try {
            labelName = a.getString(R.styleable.StatsGauge_label_name) ?: "WiFi"
            gaugeCounter = a.getInteger(R.styleable.StatsGauge_gauge_counter, 1)
        } finally {
            a.recycle()
        }
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : this(context, attrs) {}

    private val DEFAULT_WIDTH = 100

    private val progressPaint = Paint()
    private val progressPaintBlur = Paint()
    private val textPaintLabel = Paint()
    private var width1 = 0
    private var height1 = 0
    private var stroke = 0f

    var labelName = "WiFi"
        set(value) {
            field = value
            invalidate()
        }
    private var gaugeCounter = 1

    lateinit var rectF: RectF
    lateinit var rectF2: RectF
    lateinit var rectF3: RectF
    lateinit var rectF4: RectF
    private val blurMask = BlurMaskFilter(20f, BlurMaskFilter.Blur.OUTER)

    private var labelBounds = Rect()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var width = 0
        when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.AT_MOST, MeasureSpec.EXACTLY -> width =
                MeasureSpec.getSize(widthMeasureSpec)
            MeasureSpec.UNSPECIFIED -> width = DEFAULT_WIDTH
        }
        width1 = width / gaugeCounter
        height1 = ((width / gaugeCounter)*0.9).toInt()
        stroke = (width1/100*3.7).toFloat()
        setMeasuredDimension(width1, height1)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        rectF =
            RectF(0f, w.toFloat() * 0.1f, w.toFloat() * 1f, w.toFloat() * 1.1f)

        rectF2 =
            RectF(w.toFloat() * 0.15f, w.toFloat() * 0.25f, w.toFloat() * 0.85f, w.toFloat() * 0.95f)

        rectF3 =
            RectF(w.toFloat() * 0.3f, w.toFloat() * 0.4f, w.toFloat() * 0.7f, w.toFloat() * 0.8f)

        rectF4 =
            RectF(w.toFloat() * 0.48f, w.toFloat() * 0.56f, w.toFloat() * 0.52f, w.toFloat() * 0.60f)
        invalidate()
    }

    public override fun onDraw(canvas: Canvas) {
        progressPaint.strokeWidth = stroke

        progressPaintBlur.set(progressPaint)
        progressPaintBlur.maskFilter = blurMask
        progressPaintBlur.strokeWidth = stroke

        canvas.drawArc(rectF, 225f, 90f, false, progressPaint)
        canvas.drawArc(rectF, 225f, 90f, false, progressPaintBlur)

        canvas.drawArc(rectF2, 225f, 90f, false, progressPaint)
        canvas.drawArc(rectF2, 225f, 90f, false, progressPaintBlur)

        canvas.drawArc(rectF3, 225f, 90f, false, progressPaint)
        canvas.drawArc(rectF3, 225f, 90f, false, progressPaintBlur)

        canvas.drawArc(rectF4, 225f, 360f, false, progressPaint)
        canvas.drawArc(rectF4, 225f, 360f, false, progressPaintBlur)

        textPaintLabel.textSize = resources.displayMetrics.scaledDensity * 6 * width1 / 100
        textPaintLabel.getTextBounds(labelName, 0, labelName.length, labelBounds)
        val heightLabel = labelBounds.height()
        val widthLabel = labelBounds.width()

        canvas.drawText(
            labelName,
            (width1 / 2).toFloat() - widthLabel / 2,
            (height1 * 0.95).toFloat() - heightLabel / 2,
            textPaintLabel
        )
    }

    init {
        progressPaint.isAntiAlias = true
        progressPaint.isDither = true
        progressPaint.color = context.getColor(R.color.cyan_light)
        progressPaint.style = Paint.Style.STROKE
        progressPaint.strokeJoin = Paint.Join.ROUND
        progressPaint.strokeCap = Paint.Cap.ROUND

        progressPaintBlur.isAntiAlias = true

        textPaintLabel.isAntiAlias = true
        textPaintLabel.isDither = true
        textPaintLabel.color = context.getColor(R.color.grey_text)
        textPaintLabel.textSize = resources.displayMetrics.scaledDensity * 10

        ResourcesCompat.getFont(context, R.font.barlow)?.let {
            val typeface = Typeface.create(it, 300, false)
            textPaintLabel.typeface = typeface
        }
    }
}