package uk.co.symec.optimise2.worker

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.content.ContextCompat
import androidx.hilt.work.HiltWorker
import androidx.work.*
import com.google.android.gms.location.LocationServices
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import uk.co.symec.optimise2.di.AppModule
import uk.co.symec.optimise2.domain.model.*
import uk.co.symec.optimise2.domain.usecase.AddTestResultUseCase
import uk.co.symec.optimise2.domain.usecase.GetMemoryStatsUseCase
import uk.co.symec.optimise2.domain.usecase.GetStorageStatsUseCase
import java.util.*

@HiltWorker
public class TestWorker @AssistedInject constructor(
    @Assisted val ctx: Context,
    @Assisted params: WorkerParameters,
    private val testResultUseCase: AddTestResultUseCase,
    private val getStorageStatsUseCase: GetStorageStatsUseCase,
    private val getMemoryStatsUseCase: GetMemoryStatsUseCase
) : CoroutineWorker(ctx, params) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        println("Test worker started")

        val gps = AppModule.getGPSState()
        if (gps.latitude == GPSStatsModel.emptyLatitude) {
            if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
            ) {
                val fusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)
                fusedLocationClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        location?.let {
                            AppModule.getGPSState().let { gps ->
                                gps.date = Date()
                                it.let { loc ->
                                    gps.latitude = loc.latitude
                                    gps.longitude = loc.longitude
                                }
                            }
                        }
                    }
            }
        }


        val screenOn = AppModule.getScreenOn()
        var storage = StorageStatsModel.getEmptyModel()
        getStorageStatsUseCase().onSuccess {
            storage = it
            withContext(Dispatchers.Main) {
                AppModule.getStorageStats().value = it
            }
        }.onFailure { }

        var memory = MemoryStatsModel.getEmptyModel()
        getMemoryStatsUseCase().onSuccess {
            memory = it
            withContext(Dispatchers.Main) {
                AppModule.getRamStats().value = it
            }
        }.onFailure { }

        val result = TestResultModel(
            Date(),
            gps.longitude,
            gps.latitude,
            screenOn,
            storage.free,
            storage.total,
            memory.free,
            memory.total
        )

        testResultUseCase(result).onSuccess {
            println("saved")
        }.onFailure {
            println("failure")
        }

        println("Worker test result")
        return@withContext try {
            // Do something
            Result.success()
        } catch (error: Throwable) {
            Result.failure()
        }
    }
}