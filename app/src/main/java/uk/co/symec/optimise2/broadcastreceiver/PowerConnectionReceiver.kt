package uk.co.symec.optimise2.broadcastreceiver

import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.optimise2.di.AppModule

@AndroidEntryPoint
class PowerConnectionReceiver : HiltBroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        val result = when (intent.action) {
            Intent.ACTION_POWER_CONNECTED -> true
            else -> false
        }
        AppModule.setPowerConnected(result)
    }
}