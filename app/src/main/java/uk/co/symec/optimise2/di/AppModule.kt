package uk.co.symec.optimise2.di

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.work.WorkManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uk.co.symec.optimise2.data.utils.CoroutineContextProvider
import uk.co.symec.optimise2.domain.model.*
import java.util.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    enum class State { BACKGROUND, FOREGROUND }

    private var state = State.FOREGROUND

    private var isInForeground = true

    private var isScreenOn = true

    private var isPowerConnected = MutableLiveData(true)

    private var wifiStats = MutableLiveData(WifiStatsModel(Date(), false, 0, "", ""))

    private var mobileStats = MutableLiveData(MobileStatsModel(Date(), false, 0, "", ""))

    private var ramStats = MutableLiveData(MemoryStatsModel(Date(), 0, 0))

    private var storageStats = MutableLiveData(StorageStatsModel(Date(), 0, 0))

    private var gpsStats =
        GPSStatsModel(Date(), GPSStatsModel.emptyLongitude, GPSStatsModel.emptyLatitude)

    @Singleton
    @Provides
    fun provideCoroutineContext(): CoroutineContextProvider {
        return CoroutineContextProvider()
    }

    @Provides
    @Singleton
    fun provideAnalytics(@ApplicationContext context: Context): FirebaseAnalytics {
        return FirebaseAnalytics.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideWorkManager(@ApplicationContext context: Context): WorkManager {
        return WorkManager.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideCrashlytics(): FirebaseCrashlytics {
        return FirebaseCrashlytics.getInstance()
    }

    @Provides
    @Singleton
    fun isInForeground() = isInForeground

    @Provides
    @Singleton
    fun setState(value: State): State {
        isInForeground = value == State.FOREGROUND
        state = value
        return state
    }

    @Provides
    @Singleton
    fun setScreenOn(value: Boolean): Boolean {
        isScreenOn = value
        return isScreenOn
    }


    @Provides
    @Singleton
    fun getScreenOn() = isScreenOn

    @Provides
    @Singleton
    fun setPowerConnected(value: Boolean): MutableLiveData<Boolean> {
        isPowerConnected.value = value
        return isPowerConnected
    }

    @Provides
    @Singleton
    fun getPowerConnected() = isPowerConnected

    @Provides
    @Singleton
    fun getWifiState() = wifiStats

    @Provides
    @Singleton
    fun getMobileState() = mobileStats

    @Provides
    @Singleton
    fun getRamStats() = ramStats

    @Provides
    @Singleton
    fun getStorageStats() = storageStats

    @Provides
    @Singleton
    fun getGPSState() = gpsStats
}