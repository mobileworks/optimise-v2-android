package uk.co.symec.optimise2.worker

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Looper
import androidx.core.content.ContextCompat
import androidx.hilt.work.HiltWorker
import androidx.work.*
import com.google.android.gms.location.*
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import uk.co.symec.optimise2.di.AppModule
import java.util.*

@HiltWorker
public class GpsWorker @AssistedInject constructor(
    @Assisted val ctx: Context,
    @Assisted params: WorkerParameters
) : CoroutineWorker(ctx, params) {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    var counter = 0

    private fun createLocationRequest(): LocationRequest {
        return LocationRequest.create().apply {
            interval = 1000
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            numUpdates = 2
            //smallestDisplacement = 20f
        }
    }

    override suspend fun doWork(): Result = withContext(Dispatchers.Main) {
        println("Worker started")

        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION)
            + ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    locationResult.lastLocation.let {
                        counter++
                        AppModule.getGPSState().let { gps ->
                            gps.date = Date()
                            it.let { loc ->
                                gps.latitude = loc.latitude
                                gps.longitude = loc.longitude
                                println("location = ${loc.longitude}, ${loc.latitude}")
                            }
                        }
                        if (counter == 2) {
                            fusedLocationClient.removeLocationUpdates(locationCallback)
                        }
                    }
                }
            }

            fusedLocationClient.requestLocationUpdates(
                createLocationRequest(),
                locationCallback,
                Looper.getMainLooper()
            )
        }

        println("Gps worker result")
        return@withContext try {
            // Do something
            Result.success()
        } catch (error: Throwable) {
            Result.failure()
        }
    }
}