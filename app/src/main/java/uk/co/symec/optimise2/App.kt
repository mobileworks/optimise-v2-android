package uk.co.symec.optimise2

import android.app.Application
import android.content.Intent
import android.content.IntentFilter
import androidx.hilt.work.HiltWorkerFactory
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.work.Configuration
import androidx.work.WorkManager
import dagger.hilt.android.HiltAndroidApp
import uk.co.symec.optimise2.broadcastreceiver.BatteryReceiver
import uk.co.symec.optimise2.broadcastreceiver.PowerConnectionReceiver
import uk.co.symec.optimise2.broadcastreceiver.ScreenOnOffReceiver
import uk.co.symec.optimise2.di.AppModule
import uk.co.symec.optimise2.utils.ConnectivityUtils
import javax.inject.Inject

@HiltAndroidApp
class App : Application(), LifecycleObserver, Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

    override fun onCreate() {
        super.onCreate()

        WorkManager.initialize(this, workManagerConfiguration)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        registerScreenOnOffBroadcastReceiver()
        registerPowerConnectionBroadcastReceiver()
        registerReceiver(BatteryReceiver(), IntentFilter(Intent.ACTION_BATTERY_CHANGED))

        ConnectivityUtils.setConnectivityCallbacks(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        //App in background
        AppModule.setState(AppModule.State.BACKGROUND)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        // App in foreground
        AppModule.setState(AppModule.State.FOREGROUND)
    }

    private fun registerScreenOnOffBroadcastReceiver() {
        val intentFilter = IntentFilter().apply {
            addAction(Intent.ACTION_SCREEN_OFF)
            addAction(Intent.ACTION_USER_PRESENT)
        }
        registerReceiver(ScreenOnOffReceiver(), intentFilter)
    }

    private fun registerPowerConnectionBroadcastReceiver() {
        val intentFilter = IntentFilter().apply {
            addAction(Intent.ACTION_POWER_CONNECTED)
            addAction(Intent.ACTION_POWER_DISCONNECTED)
        }
        registerReceiver(PowerConnectionReceiver(), intentFilter)
    }
}