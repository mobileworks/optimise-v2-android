package uk.co.symec.optimise2.utils

const val GPS_WORKER = "gps_worker"
const val TAG_GPS_WORKER = "TAG_GPS_WORKER"

const val TEST_WORKER = "test_worker"
const val TAG_TEST_WORKER = "TAG_TEST_WORKER"

const val RAM_USAGE = "ram_usage"
const val STORAGE_USAGE = "storage_usage"

//const val MOBILE_PROVIDER = "mobileProvider"
//const val MOBILE_AVAILABLE = "mobileAvailable"
//const val MOBILE_STRENGTH = "mobileStrenght"