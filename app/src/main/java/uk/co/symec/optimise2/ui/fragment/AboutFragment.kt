package uk.co.symec.optimise2.ui.fragment

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.optimise2.data.di.NetworkModule
import uk.co.symec.optimise2.databinding.AboutFragmentBinding
import uk.co.symec.optimise2.vm.MainViewModel


@AndroidEntryPoint
class AboutFragment : BottomSheetDialogFragment() {

    companion object {
        fun newInstance() = AboutFragment().apply {
            arguments = Bundle().apply {
            }
        }
    }

    private val parentViewModel: MainViewModel by activityViewModels()
    private var _binding: AboutFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var contentId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        _binding = AboutFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        NetworkModule.getAssetId(requireContext())?.let {
            binding.tvAssetId.text = it
        }

        NetworkModule.getCustomerId(requireContext())?.let {
            binding.tvCustomerId.text = it
        }

        try {
            val pInfo: PackageInfo = requireActivity().packageManager.getPackageInfo(
                requireContext().applicationContext.packageName,
                0
            )
            binding.tvAppVersion.text = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }
}