package uk.co.symec.optimise2.ui.fragment

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface.BOLD
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.optimise2.R
import uk.co.symec.optimise2.databinding.PermissionsFragmentBinding
import uk.co.symec.optimise2.utils.extensions.snackbar
import uk.co.symec.optimise2.vm.MainViewModel
import uk.co.symec.optimise2.vm.PermissionsViewModel


@AndroidEntryPoint
class PermissionsFragment : BottomSheetDialogFragment() {

    companion object {
        fun newInstance(cancel: Boolean) = PermissionsFragment().apply {
            isCancelable = cancel
//            arguments = Bundle().apply {
//            }
        }
    }

    private val viewModel: PermissionsViewModel by viewModels()
    private val parentViewModel: MainViewModel by activityViewModels()
    private var _binding: PermissionsFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var contentId: String

    private var showDisablePower = true

    private val requestPermissionsLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { grantedList ->

            val askForPermission = mutableListOf<String>()
            grantedList.forEach { (name, granted) ->
                if (!granted) {
                    askForPermission.add(name)
                }
            }

            val permissionsGranted = askForPermission.isEmpty()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle(requireContext().getString(R.string.background_location_permission_title))
                    builder.setMessage(requireContext().getString(R.string.background_location_permission_message))
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                        packageManager.backgroundPermissionOptionLabel
//                    }
                    builder.setNegativeButton(R.string.no) { dialog, _ ->
                        disablePowerOptimisation()
                        dialog.dismiss()
                    }
                    builder.setPositiveButton(R.string.yes) { _, _ ->
                        requestPermissionLauncher.launch(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                    }
                    builder.show()
                }
            }

            when {
                permissionsGranted -> {
                    parentViewModel.startWorker(100)
                    parentViewModel.startWorkerTest(100)
                    viewModel.checkPermissions(requireContext())
                }
                else -> {
                    Toast.makeText(requireContext(), "Permission denied", Toast.LENGTH_LONG).show()
                }
            }
            disablePowerOptimisation()
        }

    val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) {
                parentViewModel.startWorker(100)
                parentViewModel.startWorkerTest(100)
                viewModel.checkPermissions(requireContext())
            } else {
                Toast.makeText(requireContext(), "Permission denied", Toast.LENGTH_LONG).show()
            }
            disablePowerOptimisation()
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        _binding = PermissionsFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.checkPermissions(requireContext())

        binding.btnGrantPermissions.setOnClickListener {
            if (parentViewModel.checkPermissions(requireContext())) {
                Toast.makeText(requireContext(), R.string.all_permissions_granted, Toast.LENGTH_LONG).show()
            } else {
                checkPermissionsButton()
            }
        }

        viewModel.phoneState.observe(viewLifecycleOwner, {
            if (it) {
                enabled(binding.tvPhonePermissionsButton)
            } else {
                disabled(binding.tvPhonePermissionsButton)
            }
        })

        viewModel.locationState.observe(viewLifecycleOwner, {
            if (it) {
                enabled(binding.tvLocationPermissionsButton)
            } else {
                disabled(binding.tvLocationPermissionsButton)
            }
        })

        viewModel.powerState.observe(viewLifecycleOwner, {
            if (it) {
                enabled(binding.tvPowerPermissionsButton)
            } else {
                disabled(binding.tvPowerPermissionsButton)
            }
        })

        viewModel.backgroundState.observe(viewLifecycleOwner, {
            if (it) {
                enabled(binding.tvBackgroundPermissionsButton)
            } else {
                disabled(binding.tvBackgroundPermissionsButton)
            }
        })

        val string = SpannableStringBuilder(resources.getString(R.string.permissions_text))
        string.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.cyan)),
            string.length - 1,
            string.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        string.setSpan(
            StyleSpan(BOLD),
            string.length - 10,
            string.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        binding.tvPermissionsText.text = string

        dialog?.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
        }
    }

    private fun checkPermissionsButton() {
        showDisablePower = true
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
                    + ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                    + ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
                    + ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_PHONE_STATE
            )
                    == PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
                parentViewModel.startWorker(100)
                parentViewModel.startWorkerTest(100)
            }
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                    + ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
                    + ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_PHONE_STATE
            )
                    != PackageManager.PERMISSION_GRANTED -> {
                requestPermissionsLauncher.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE
                    )
                )
            }
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION) -> {
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle(requireContext().getString(R.string.background_location_permission_title))
                builder.setMessage(requireContext().getString(R.string.background_location_permission_message))

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    //                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    //                        packageManager.backgroundPermissionOptionLabel
                    //                    }
                    builder.setNegativeButton(R.string.no) { dialog, _ ->
                        disablePowerOptimisation()
                        dialog.dismiss()
                    }

                    builder.setPositiveButton(R.string.yes) { _, _ ->
                        requestPermissionsLauncher.launch(arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION))
                    }
                }
                builder.show()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE) -> {
                snackbar("Show more info!", binding.root)
                requestPermissionLauncher.launch(Manifest.permission.READ_PHONE_STATE)
            }
            else -> {
                requestPermissionsLauncher.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE
                    )
                )
            }
        }
    }

    private fun disablePowerOptimisation() {
        if (showDisablePower) {
            (requireContext().getSystemService(Context.POWER_SERVICE) as PowerManager).let { powerManager ->
                if (!powerManager.isIgnoringBatteryOptimizations(requireContext().packageName)) {
                    //show intent battery optimization settings
                    Intent().apply {
                        action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                        data = Uri.parse("package:" + requireActivity().packageName)
                        startActivity(this)
                    }
                    showDisablePower = false
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.checkPermissions(requireContext())
        if (parentViewModel.checkPermissions(requireContext())) {
            isCancelable = true
        }
    }

    private fun enabled(tv: TextView) {
        tv.setText(R.string.enabled)
        tv.setTextColor(ContextCompat.getColor(requireContext(), R.color.cyan))
        tv.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.enabled_permission_background)
    }

    private fun disabled(tv: TextView) {
        tv.setText(R.string.disabled)
        tv.setTextColor(ContextCompat.getColor(requireContext(), R.color.grey_text))
        tv.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.disabled_permission_background)
    }
}