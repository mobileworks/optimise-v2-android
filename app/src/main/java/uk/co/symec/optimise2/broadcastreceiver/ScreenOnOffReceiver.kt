package uk.co.symec.optimise2.broadcastreceiver

import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uk.co.symec.optimise2.di.AppModule
import uk.co.symec.optimise2.domain.model.onFailure
import uk.co.symec.optimise2.domain.model.onSuccess
import uk.co.symec.optimise2.domain.usecase.AddScreenOnUseCase
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ScreenOnOffReceiver : HiltBroadcastReceiver() {

    @Inject
    lateinit var addScreenOnUseCase: AddScreenOnUseCase

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        val result = when (intent.action) {
            Intent.ACTION_USER_PRESENT -> true
            else -> false
        }
        AppModule.setScreenOn(result)
        addScreenOn(result, AppModule.isInForeground(), context)
    }

    private fun addScreenOn(screenOn: Boolean, foregroundOn: Boolean, context: Context) {
        GlobalScope.launch {
            addScreenOnUseCase(Date(), screenOn, foregroundOn)
                .onSuccess {
                    //if ()
                    //showToast(context, context.getString(R.string.file_downloaded, it))
                }
                .onFailure {
                    //showToast(context, it.throwable.message ?: "Error")
                }
        }
    }
}