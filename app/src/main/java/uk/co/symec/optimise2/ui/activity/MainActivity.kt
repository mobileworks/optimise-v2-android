package uk.co.symec.optimise2.ui.activity

import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.optimise2.R
import uk.co.symec.optimise2.databinding.ActivityMainBinding
import uk.co.symec.optimise2.di.AppModule
import uk.co.symec.optimise2.domain.model.*
import uk.co.symec.optimise2.ui.base.*
import uk.co.symec.optimise2.ui.base.Success
import uk.co.symec.optimise2.ui.fragment.AboutFragment
import uk.co.symec.optimise2.ui.fragment.ImprovementsFragment
import uk.co.symec.optimise2.ui.fragment.PermissionsFragment
import uk.co.symec.optimise2.utils.*
import uk.co.symec.optimise2.vm.MainViewModel
import java.util.*


@AndroidEntryPoint
class MainActivity : BaseActivity() {

    val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)

        val locationRequest = LocationRequest.create().apply {
            interval = 1000
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { _ ->
            checkPermissions()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(
                        this@MainActivity,
                        102
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }

        binding.tvBatteryText.text = getText(R.string.left_to_discharged)
        viewModel.getLastResult()
        subscribeToData()
    }

    private fun subscribeToData() {
        viewModel.resultState.observe(this, { vs ->
            handleResultState(vs)
        })

        viewModel.batteryState.observe(this, { vs ->
            handleBatteryState(vs)
        })

        viewModel.improvementState.observe(this, { vs ->
            handleImprovementState(vs)
        })

//        viewModel.getBatteryHealth()
//        viewModel.getScreenOnTime()

        AppModule.getWifiState().observe(this, {
            if (it.isAvailable) {
                binding.sgvSignal.percentage = it.getPercentage()
                binding.svWifi.labelName = resources.getString(R.string.wifi)
                binding.cnvSsid.labelProvider = it.name ?: ""
                binding.cnvSsid.labelName = resources.getString(R.string.ssid)
                binding.clConnection.visibility = View.VISIBLE

                binding.sgvSignal.invalidate()
                binding.svWifi.invalidate()
                binding.cnvSsid.invalidate()
            } else {
                if (AppModule.getMobileState().value?.isAvailable == true) {
                    binding.clConnection.visibility = View.VISIBLE
                } else {
                    binding.clConnection.visibility = View.GONE
                }
            }
            viewModel.updateWifiModel(it)
        })

        AppModule.getMobileState().observe(this, {
            if (it.isAvailable) {
                binding.sgvSignal.percentage = it.getPercentage()
                binding.svWifi.labelName = it.type ?: ""
                binding.cnvSsid.labelProvider = it.name ?: ""
                binding.cnvSsid.labelName = resources.getString(R.string.provider)
                binding.clConnection.visibility = View.VISIBLE

                binding.sgvSignal.invalidate()
                binding.svWifi.invalidate()
                binding.cnvSsid.invalidate()
            } else {
                if (AppModule.getWifiState().value?.isAvailable == true) {
                    binding.clConnection.visibility = View.VISIBLE
                } else {
                    binding.clConnection.visibility = View.GONE
                }
            }
            viewModel.updateMobileModel(it)
        })

        AppModule.getRamStats().observe(this, {
            binding.sgvRam.percentage = it.getMemoryPercentage()
            binding.sgvRam.invalidate()
            viewModel.updateRamMemory(it)
        })

        AppModule.getStorageStats().observe(this, {
            binding.sgvStorage.percentage = it.getStoragePercentage()
            binding.sgvStorage.invalidate()
            viewModel.updateStorage(it)
        })


        AppModule.getPowerConnected().observe(this, {
            binding.clCharging.visibility = when (it) {
                true -> View.VISIBLE
                else -> View.GONE
            }
        })
    }

    private fun handleResultState(viewState: ViewState<TestResultModel>) {
        when (viewState) {
            is Success -> {
                viewState.data.let {
                    binding.sgvRam.percentage = it.getMemoryPercentage()
                    binding.sgvStorage.percentage = it.getStoragePercentage()

                    viewModel.updateStorage(
                        StorageStatsModel(
                            Date(),
                            it.totalStorage,
                            it.freeStorage
                        )
                    )
                    viewModel.updateRamMemory(
                        MemoryStatsModel(
                            Date(),
                            it.totalMemory,
                            it.freeMemory
                        )
                    )
                }
            }
            //is Error -> handleError(viewState.error.localizedMessage ?: "Default error message")
            else -> {
            }
        }
    }

    private fun handleBatteryState(viewState: ViewState<BatteryInfoModel>) {
        when (viewState) {
            is Success -> {
                viewState.data.let {
                    if (it.batteryTimeLeftHours == 0 && it.batteryTimeLeftMinutes == 0) {
                        binding.tvBatteryTime.text =
                            resources.getString(R.string.battery_time_label_no_data)
                    } else {
                        binding.tvBatteryTime.text = resources.getString(
                            R.string.battery_time_label,
                            it.batteryTimeLeftHours,
                            it.batteryTimeLeftMinutes
                        )
                    }
                    binding.ivBatteryView.setImageDrawable(getBatteryImage(it.batteryLevel))
                    binding.clCharging.visibility = when (it.status) {
                        "Charging", "Full" -> View.VISIBLE
                        else -> View.GONE
                    }
                }
            }
            //is Error -> handleError(viewState.error.localizedMessage ?: "Default error message")
            else -> {
            }
        }
    }

    private fun handleImprovementState(viewState: ImprovementState<ImprovementModel>) {
        when (viewState) {
            is AllOk -> {
                binding.clImprovements.visibility = View.GONE
                binding.tvTitle.text = getText(R.string.everything_is_fine)
            }
            is ShowWarning -> {
                binding.clImprovements.visibility = View.VISIBLE
                viewState.data.let { model ->
                    binding.tvWarningImprovements.visibility = View.VISIBLE
                    binding.tvWarningImprovements.text = model.noOfWarning.toString()

                    binding.tvCriticalImprovements.visibility = View.GONE

                    binding.btnViewImprovements.setOnClickListener {
                        val fragment = ImprovementsFragment.newInstance(model)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                    binding.tvTitle.text = getText(R.string.device_need_improvements)
                }
            }
            is ShowCritical -> {
                binding.clImprovements.visibility = View.VISIBLE
                viewState.data.let { model ->
                    binding.tvCriticalImprovements.visibility = View.VISIBLE
                    binding.tvCriticalImprovements.text = model.noOfCritical.toString()

                    binding.tvWarningImprovements.visibility = View.GONE

                    binding.btnViewImprovements.setOnClickListener {
                        val fragment = ImprovementsFragment.newInstance(model)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                    binding.tvTitle.text = getText(R.string.device_need_improvements)
                }
            }
            is ShowAll -> {
                binding.clImprovements.visibility = View.VISIBLE
                viewState.data.let { model ->
                    binding.tvWarningImprovements.visibility = View.VISIBLE
                    binding.tvWarningImprovements.text = model.noOfWarning.toString()

                    binding.tvCriticalImprovements.visibility = View.VISIBLE
                    binding.tvCriticalImprovements.text = model.noOfCritical.toString()

                    binding.btnViewImprovements.setOnClickListener {
                        val fragment = ImprovementsFragment.newInstance(model)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                    binding.tvTitle.text = getText(R.string.device_need_improvements)
                }
            }
        }
    }

    private fun getBatteryImage(batteryLevel: Int): Drawable? {
        return when (batteryLevel) {
            in 0..10 -> ContextCompat.getDrawable(this, R.drawable.battery_0)
            in 11..30 -> ContextCompat.getDrawable(this, R.drawable.battery_20)
            in 31..50 -> ContextCompat.getDrawable(this, R.drawable.battery_40)
            in 51..70 -> ContextCompat.getDrawable(this, R.drawable.battery_60)
            in 71..90 -> ContextCompat.getDrawable(this, R.drawable.battery_80)
            in 91..100 -> ContextCompat.getDrawable(this, R.drawable.battery_100)
            else -> ContextCompat.getDrawable(this, R.drawable.battery_0)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBatteryInfo()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_info -> {
                customAlertDialog()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkPermissions() {
        if (!viewModel.checkPermissions(this)) {
            val fragment = PermissionsFragment.newInstance(false)
            fragment.show(supportFragmentManager, fragment.tag)
        }
    }

    private fun customAlertDialog() {
        val dialogBuilder: AlertDialog.Builder =
            AlertDialog.Builder(this, R.style.OptimiseV2_DialogTheme)
        val inflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_alert_dialog, null)
        dialogBuilder.setView(dialogView)
        val alertDialog: AlertDialog = dialogBuilder.create().apply {
            window?.apply {
                setGravity(Gravity.CENTER)
            }
        }
        dialogView.findViewById<ImageView>(R.id.iv_close)?.let {
            it.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        dialogView.findViewById<ConstraintLayout>(R.id.cl_about)?.let {
            it.setOnClickListener {
                val fragment = AboutFragment.newInstance()
                fragment.show(supportFragmentManager, fragment.tag)
            }
        }

        dialogView.findViewById<ConstraintLayout>(R.id.cl_permissions)?.let {
            it.setOnClickListener {
                val fragment = PermissionsFragment.newInstance(false)
                fragment.show(supportFragmentManager, fragment.tag)
            }
        }

        alertDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 102) {
            if (resultCode == 0) {
                val alert: AlertDialog
                val builder = AlertDialog.Builder(this)
                builder.setMessage(resources.getString(R.string.dialog_missing_gps_permissions))
                    .setCancelable(false)
                    .setNegativeButton(resources.getString(R.string.ok)) { dialog: DialogInterface?, _: Int ->
                        dialog?.dismiss()
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        applicationContext.startActivity(intent)
                    }
                alert = builder.create()
                alert.show()
            } else if (resultCode == -1) {
                checkPermissions()
            }
        }
    }
}