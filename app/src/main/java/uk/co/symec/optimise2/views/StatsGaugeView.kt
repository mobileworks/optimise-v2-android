package uk.co.symec.optimise2.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat
import uk.co.symec.optimise2.R


class StatsGaugeView : View {

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        val a = context!!.theme.obtainStyledAttributes(attrs, R.styleable.StatsGauge, 0, 0)
        try {
            percentage = a.getInteger(R.styleable.StatsGauge_percentage, 60)
            labelName = a.getString(R.styleable.StatsGauge_label_name) ?: "Test"
            gaugeCounter = a.getInteger(R.styleable.StatsGauge_gauge_counter, 1)
            warningLevel = a.getInteger(R.styleable.StatsGauge_warning_level, 1)
            criticalLevel = a.getInteger(R.styleable.StatsGauge_critical_level, 1)
            revert = a.getBoolean(R.styleable.StatsGauge_revert, false)
        } finally {
            a.recycle()
        }
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : this(context, attrs) {}

    private val DEFAULT_WIDTH = 100
    private val START_ANGLE = 160f

    private val backgroundPaint = Paint()
    private val progressPaint = Paint()
    private val progressPaintBlur = Paint()
    private val textPaintCounter = Paint()
    private val textPaintCounterBlur = Paint()
    private val textPaintLabel = Paint()
    private var width1 = 0
    private var height1 = 0
    private var stroke = 0f
    private var stroke2 = 8f

    private var labelName = String()
    var percentage = 1
        set(value) {
            field = value
            invalidate()
        }

    private var gaugeCounter = 1

    private var warningLevel = 1
    private var criticalLevel = 1
    private var revert = false

    lateinit var rectF: RectF
    private val blurMask = BlurMaskFilter(20f, BlurMaskFilter.Blur.OUTER)
    private val blurMask2 = BlurMaskFilter(40f, BlurMaskFilter.Blur.NORMAL)

    private var counterBounds = Rect()
    private var labelBounds = Rect()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var width = 0
        when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.AT_MOST, MeasureSpec.EXACTLY -> width =
                MeasureSpec.getSize(widthMeasureSpec)
            MeasureSpec.UNSPECIFIED -> width = DEFAULT_WIDTH
        }
        width1 = width / gaugeCounter
        height1 = ((width / gaugeCounter) * 0.9).toInt()
        stroke = (width1 / 100 * 3.5).toFloat()
        setMeasuredDimension(width1, height1)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        rectF =
            RectF(w.toFloat() * 0.1f, w.toFloat() * 0.1f, w.toFloat() * 0.9f, w.toFloat() * 0.9f)
        invalidate()
    }

    public override fun onDraw(canvas: Canvas) {

        if (revert) {
            when {
                percentage <= criticalLevel -> {
                    progressPaint.color = context.getColor(R.color.red)
                    backgroundPaint.color = context.getColor(R.color.red)
                }
                percentage <= warningLevel -> {
                    progressPaint.color = context.getColor(R.color.yellow)
                    backgroundPaint.color = context.getColor(R.color.yellow)
                }
                else -> {
                    progressPaint.color = context.getColor(R.color.cyan_light)
                    backgroundPaint.color = context.getColor(R.color.cyan_light)
                }
            }
        } else {
            when {
                percentage >= criticalLevel -> {
                    progressPaint.color = context.getColor(R.color.red)
                    backgroundPaint.color = context.getColor(R.color.red)
                }
                percentage >= warningLevel -> {
                    progressPaint.color = context.getColor(R.color.yellow)
                    backgroundPaint.color = context.getColor(R.color.yellow)
                }
                else -> {
                    progressPaint.color = context.getColor(R.color.teal)
                    backgroundPaint.color = context.getColor(R.color.teal)
                }
            }
        }
        backgroundPaint.alpha = 20
        backgroundPaint.strokeWidth = stroke

        progressPaint.strokeWidth = stroke

        progressPaintBlur.set(progressPaint)
        progressPaintBlur.maskFilter = blurMask
        progressPaintBlur.strokeWidth = stroke

        textPaintCounterBlur.set(progressPaint)
        textPaintCounterBlur.maskFilter = blurMask2
        textPaintCounterBlur.strokeWidth = stroke2

        val arc = percentage * 2.2f
        canvas.drawArc(rectF, START_ANGLE, 220f, false, backgroundPaint)
        canvas.drawArc(rectF, START_ANGLE, arc, false, progressPaint)
        canvas.drawArc(rectF, START_ANGLE, arc, false, progressPaintBlur)

        val text = "$percentage%"
        textPaintCounter.textSize = resources.displayMetrics.scaledDensity * 8 * width1 / 100
        textPaintCounter.getTextBounds(text, 0, text.length, counterBounds)
        val height = counterBounds.height()
        val width = counterBounds.width()

        textPaintLabel.textSize = resources.displayMetrics.scaledDensity * 6 * width1 / 100
        textPaintLabel.getTextBounds(labelName, 0, labelName.length, labelBounds)
        val heightLabel = labelBounds.height()
        val widthLabel = labelBounds.width()

        canvas.drawText(
            "$percentage%",
            (width1 / 2).toFloat() - width / 2,
            (height1 * 0.65).toFloat() - height / 2,
            textPaintCounter
        )

        canvas.drawCircle(
            (width1 / 2).toFloat(),
            (height1 * 0.45).toFloat(),
            40f,
            textPaintCounterBlur
        )

        canvas.drawText(
            labelName,
            (width1 / 2).toFloat() - widthLabel / 2,
            (height1 * 0.95).toFloat() - heightLabel / 2,
            textPaintLabel
        )
    }

    init {
        backgroundPaint.isAntiAlias = true
        backgroundPaint.isDither = true
        backgroundPaint.color = context.getColor(R.color.cyan_light)
        backgroundPaint.alpha = 20
        backgroundPaint.style = Paint.Style.STROKE
        backgroundPaint.strokeJoin = Paint.Join.ROUND
        backgroundPaint.strokeCap = Paint.Cap.ROUND

        progressPaint.isAntiAlias = true
        progressPaint.isDither = true
        progressPaint.color = context.getColor(R.color.cyan_light)
        progressPaint.style = Paint.Style.STROKE
        progressPaint.strokeJoin = Paint.Join.ROUND
        progressPaint.strokeCap = Paint.Cap.ROUND

        progressPaintBlur.isAntiAlias = true

        textPaintCounter.isAntiAlias = true
        textPaintCounter.isDither = true
        textPaintCounter.color = Color.WHITE

        ResourcesCompat.getFont(context, R.font.barlow)?.let {
            val typeface = Typeface.create(it, 200, false)
            textPaintCounter.typeface = typeface
        }

        textPaintCounter.textSize = resources.displayMetrics.scaledDensity * 10

        textPaintCounterBlur.isAntiAlias = true
        textPaintCounterBlur.style = Paint.Style.FILL

        textPaintLabel.isAntiAlias = true
        textPaintLabel.isDither = true
        textPaintLabel.color = context.getColor(R.color.grey_text)
        textPaintLabel.textSize = resources.displayMetrics.scaledDensity * 10

        ResourcesCompat.getFont(context, R.font.barlow)?.let {
            val typeface = Typeface.create(it, 300, false)
            textPaintLabel.typeface = typeface
        }
    }
}