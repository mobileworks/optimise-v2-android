package uk.co.symec.optimise2.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uk.co.symec.optimise2.databinding.ImprovementListItemBinding
import uk.co.symec.optimise2.domain.model.ImprovementItemModel

class ImprovementsListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var improvementsList = mutableListOf<ImprovementItemModel>()

    fun setImprovementsList(list: List<ImprovementItemModel>) {
        improvementsList.clear()
        improvementsList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImprovementsViewHolder.from(parent)
    }

    override fun getItemId(position: Int): Long {
        return improvementsList[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return improvementsList.size
    }

    private fun getItem(position: Int): ImprovementItemModel {
        return improvementsList[position]
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ImprovementsViewHolder -> {
                val header = getItem(position)
                holder.bind(header)
                holder.setIsRecyclable(false)
            }
        }
    }
}

class ImprovementsViewHolder private constructor(val binding: ImprovementListItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ImprovementItemModel) {
        binding.item = item

        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): ImprovementsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ImprovementListItemBinding.inflate(layoutInflater, parent, false)
            return ImprovementsViewHolder(binding)
        }
    }
}
