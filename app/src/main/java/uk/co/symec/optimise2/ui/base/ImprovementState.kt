package uk.co.symec.optimise2.ui.base

sealed class ImprovementState<out T : Any>
class AllOk<out T : Any>(val data: T) : ImprovementState<T>()
class ShowWarning<out T : Any>(val data: T) : ImprovementState<T>()
class ShowCritical<out T : Any>(val data: T) : ImprovementState<T>()
class ShowAll<out T : Any>(val data: T) : ImprovementState<T>()