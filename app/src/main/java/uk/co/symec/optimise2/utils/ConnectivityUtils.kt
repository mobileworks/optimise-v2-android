package uk.co.symec.optimise2.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Build
import android.telephony.*
import uk.co.symec.optimise2.di.AppModule
import java.util.*

class ConnectivityUtils {
    companion object {
        private fun getWifiRequest(): NetworkRequest {
            return NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build()
        }

        private fun getMobileRequest(): NetworkRequest {
            return NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .build()
        }

        private fun getConnectivityManager(context: Context) =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        fun setConnectivityCallbacks(context: Context) {
            getConnectivityManager(context).apply {
                registerNetworkCallback(getWifiRequest(), wifiCallBack(context))
                registerNetworkCallback(getMobileRequest(), mobileCallBack(context))
            }
        }

        private fun wifiCallBack(context: Context): ConnectivityManager.NetworkCallback {
            return object : ConnectivityManager.NetworkCallback() {
                override fun onCapabilitiesChanged(
                    network: Network,
                    networkCapabilities: NetworkCapabilities
                ) {
                    super.onCapabilitiesChanged(network, networkCapabilities)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        networkCapabilities.apply {
                            AppModule.getWifiState().value?.signalStrength = signalStrength
                        }
                        (context.applicationContext.getSystemService(
                            Context.WIFI_SERVICE
                        ) as? WifiManager)?.let {
                            it.connectionInfo?.apply {
                                AppModule.getWifiState().value?.let { wfsm ->
                                    val ssid = this.ssid.replace("\"", "")
                                    wfsm.name = ssid
                                    wfsm.type = this.frequency.toString()
                                    wfsm.date = Date()

                                    AppModule.getWifiState().postValue(wfsm)
                                }
                            }
                        }
                    } else {
                        (context.applicationContext.getSystemService(
                            Context.WIFI_SERVICE
                        ) as? WifiManager)?.let {
                            it.connectionInfo?.apply {
                                AppModule.getWifiState().value?.let { wfsm ->
                                    wfsm.signalStrength = this.rssi
                                    val ssid = this.ssid.replace("\"", "")
                                    wfsm.name = ssid
                                    wfsm.type = this.frequency.toString()
                                    wfsm.date = Date()

                                    AppModule.getWifiState().postValue(wfsm)
                                }
                            }
                        }
                    }
                }

                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    AppModule.getWifiState().value?.let { wfsm ->
                        wfsm.isAvailable = true
                        wfsm.date = Date()

                        AppModule.getWifiState().postValue(wfsm)
                    }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    AppModule.getWifiState().value?.let { wfsm ->
                        wfsm.isAvailable = false
                        wfsm.date = Date()

                        AppModule.getWifiState().postValue(wfsm)
                    }
                }
            }
        }

        private fun mobileCallBack(context: Context): ConnectivityManager.NetworkCallback {
            return object : ConnectivityManager.NetworkCallback() {
                override fun onCapabilitiesChanged(
                    network: Network,
                    networkCapabilities: NetworkCapabilities
                ) {
                    super.onCapabilitiesChanged(network, networkCapabilities)
                    val tel =
                        context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                    try {
                        AppModule.getMobileState().value?.let { msm ->
                            msm.type = getMobileConnectionType(tel.dataNetworkType)
                            msm.name =  tel.networkOperatorName
                            msm.signalStrength = getSignalStrength(tel)
                            msm.date = Date()

                            AppModule.getMobileState().postValue(msm)
                        }
                    } catch (e: SecurityException) {

                    }
                }

                private fun getSignalStrength(tel: TelephonyManager): Int {
                    var bestValue = Int.MIN_VALUE
                    var value: Int
                    try {
                        tel.allCellInfo.forEach { current ->
                            when (current) {
                                is CellInfoGsm -> {
                                    value = current.cellSignalStrength.dbm
                                    println(value)
                                    if (value > bestValue) {
                                        bestValue = value
                                    }
                                }
                                is CellInfoCdma -> {
                                    value = current.cellSignalStrength.dbm
                                    println(value)
                                    if (value > bestValue) {
                                        bestValue = value
                                    }
                                }
                                is CellInfoLte -> {
                                    value = current.cellSignalStrength.dbm
                                    println(value)
                                    if (value > bestValue) {
                                        bestValue = value
                                    }
                                }
                                is CellInfoWcdma -> {
                                    value = current.cellSignalStrength.dbm
                                    println(value)
                                    if (value > bestValue) {
                                        bestValue = value
                                    }
                                }
                                else -> 0
                            }
                        }
                    } catch (e: SecurityException) {
                        //
                    }
                    return bestValue
                }

                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    AppModule.getMobileState().value?.let { ms ->
                        ms.isAvailable = true
                        ms.date = Date()

                        AppModule.getMobileState().postValue(ms)
                    }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    AppModule.getMobileState().value?.let { ms ->
                        ms.isAvailable = false
                        ms.date = Date()

                        AppModule.getMobileState().postValue(ms)
                    }
                }
            }
        }

        private fun getMobileConnectionType(dataNetworkType: Int): String {
            return when (dataNetworkType) {
                TelephonyManager.NETWORK_TYPE_CDMA,
                TelephonyManager.NETWORK_TYPE_IDEN,
                TelephonyManager.NETWORK_TYPE_GPRS -> "2G"
                TelephonyManager.NETWORK_TYPE_1xRTT,
                TelephonyManager.NETWORK_TYPE_EDGE -> "2,5G"
                TelephonyManager.NETWORK_TYPE_UMTS,
                TelephonyManager.NETWORK_TYPE_EVDO_0,
                TelephonyManager.NETWORK_TYPE_EVDO_A,
                TelephonyManager.NETWORK_TYPE_EVDO_B,
                TelephonyManager.NETWORK_TYPE_HSDPA,
                TelephonyManager.NETWORK_TYPE_HSUPA,
                TelephonyManager.NETWORK_TYPE_HSPA,
                TelephonyManager.NETWORK_TYPE_EHRPD -> "3G"
                TelephonyManager.NETWORK_TYPE_HSPAP -> "3,5G"
                TelephonyManager.NETWORK_TYPE_LTE -> "4G"
                TelephonyManager.NETWORK_TYPE_NR -> "5G"
                else -> "?"
            }
        }
    }
}