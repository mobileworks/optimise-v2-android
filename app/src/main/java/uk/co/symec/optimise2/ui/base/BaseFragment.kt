package uk.co.symec.optimise2.ui.base

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import uk.co.symec.optimise2.utils.extensions.getSharedPreferences

abstract class BaseFragment<T : ViewDataBinding> : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewReady()
    }

    protected fun onBackPressed() = (activity as BaseActivity).onBackPressed()

    abstract fun viewReady()

    open fun showError(@StringRes errorMessage: Int, rootView: View) {
        (activity as BaseActivity).showError(errorMessage, rootView)
    }

    open fun showError(errorMessage: String?, rootView: View) {
        (activity as BaseActivity).showError(errorMessage, rootView)
    }

    open fun showLoading(loading: ConstraintLayout) {
        (activity as BaseActivity).showLoading(loading)
    }

    open fun hideLoading(loading: ConstraintLayout) {
        (activity as BaseActivity).hideLoading(loading)
    }

    open fun getSharedPreferences(): SharedPreferences {
        return (activity as BaseActivity).getSharedPreferences()
    }
}