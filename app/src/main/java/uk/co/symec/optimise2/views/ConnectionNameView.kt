package uk.co.symec.optimise2.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat
import uk.co.symec.optimise2.R


class ConnectionNameView : View {

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        val a = context!!.theme.obtainStyledAttributes(attrs, R.styleable.StatsGauge, 0, 0)
        try {
            labelName = a.getString(R.styleable.StatsGauge_label_name) ?: "WiFi"
            gaugeCounter = a.getInteger(R.styleable.StatsGauge_gauge_counter, 1)
        } finally {
            a.recycle()
        }
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : this(context, attrs) {}

    private val DEFAULT_WIDTH = 100

    private val textPaintLabel = Paint()
    private val textPaintCounter = Paint()
    private var width1 = 0
    private var height1 = 0
    private var stroke = 0f

    var labelName = ""
        set(value) {
            field = value
            invalidate()
        }

    var labelProvider = ""
        set(value) {
            field = value
            invalidate()
        }
    private var gaugeCounter = 1

    private var counterBounds = Rect()
    private var labelBounds = Rect()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var width = 0
        when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.AT_MOST, MeasureSpec.EXACTLY -> width =
                MeasureSpec.getSize(widthMeasureSpec)
            MeasureSpec.UNSPECIFIED -> width = DEFAULT_WIDTH
        }
        width1 = width / gaugeCounter
        height1 = ((width / gaugeCounter) * 0.9).toInt()
        stroke = (width1 / 100 * 3.7).toFloat()
        setMeasuredDimension(width1, height1)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        invalidate()
    }

    public override fun onDraw(canvas: Canvas) {

        textPaintCounter.textSize = resources.displayMetrics.scaledDensity * 5 * width1 / 100
        textPaintCounter.getTextBounds(labelProvider, 0, labelProvider.length, counterBounds)
        val height = counterBounds.height()
        val width = counterBounds.width()

        canvas.drawText(
            labelProvider,
            (width1 / 2).toFloat() - width / 2,
            (height1 * 0.65).toFloat() - height / 2,
            textPaintCounter
        )

        textPaintLabel.textSize = resources.displayMetrics.scaledDensity * 6 * width1 / 100
        textPaintLabel.getTextBounds(labelName, 0, labelName.length, labelBounds)
        val heightLabel = labelBounds.height()
        val widthLabel = labelBounds.width()

        canvas.drawText(
            labelName,
            (width1 / 2).toFloat() - widthLabel / 2,
            (height1 * 0.95).toFloat() - heightLabel / 2,
            textPaintLabel
        )
    }

    init {
        textPaintCounter.isAntiAlias = true
        textPaintCounter.isDither = true
        textPaintCounter.color = Color.WHITE
        textPaintCounter.textSize = resources.displayMetrics.scaledDensity * 10

        ResourcesCompat.getFont(context, R.font.barlow)?.let {
            val typeface = Typeface.create(it, 400, false)
            textPaintCounter.typeface = typeface
        }

        textPaintLabel.isAntiAlias = true
        textPaintLabel.isDither = true
        textPaintLabel.color = context.getColor(R.color.grey_text)
        textPaintLabel.textSize = resources.displayMetrics.scaledDensity * 10

        ResourcesCompat.getFont(context, R.font.barlow)?.let {
            val typeface = Typeface.create(it, 300, false)
            textPaintLabel.typeface = typeface
        }
    }
}