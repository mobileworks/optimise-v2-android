package uk.co.symec.optimise2.broadcastreceiver

import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uk.co.symec.optimise2.domain.model.onFailure
import uk.co.symec.optimise2.domain.model.onSuccess
import uk.co.symec.optimise2.domain.usecase.AddBatteryUsageUseCase
import java.util.*
import javax.inject.Inject

const val TIME = 1

@AndroidEntryPoint
class BatteryReceiver : HiltBroadcastReceiver() {

    @Inject
    lateinit var addBatteryUsageUseCase: AddBatteryUsageUseCase

    var time = 0L
    var previousTime = 0L
    var previousStatus: String? = null
    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        try {
            time = System.currentTimeMillis()
            if (previousTime == 0L) {
                previousTime = time
            }

            val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
            val batteryStatus = when (status) {
                BatteryManager.BATTERY_STATUS_CHARGING -> "Charging"
                BatteryManager.BATTERY_STATUS_DISCHARGING -> "Discharging"
                BatteryManager.BATTERY_STATUS_FULL -> "Full"
                BatteryManager.BATTERY_STATUS_NOT_CHARGING -> "Not Charging"
                BatteryManager.BATTERY_STATUS_UNKNOWN -> "Unknow"
                else -> "No Data"
            }

            if (previousStatus == null) {
                previousStatus = batteryStatus
            }

            if (previousStatus != batteryStatus || ((previousTime + (1000 * TIME)) < time)) {
                previousTime = time
                previousStatus = batteryStatus

                val battLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
                val temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1)
                val voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1)

                val volt: Float = if (voltage > 1000)
                    voltage / 1000f
                else
                    voltage.toFloat()

                val chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
                val battPowerSource = when (chargePlug) {
                    BatteryManager.BATTERY_PLUGGED_AC -> "AC"
                    BatteryManager.BATTERY_PLUGGED_USB -> "USB"
                    BatteryManager.BATTERY_PLUGGED_WIRELESS -> "Wireless"
                    else -> "No Data"
                }

                val bHealth = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1)
                val batteryHealth = when (bHealth) {
                    BatteryManager.BATTERY_HEALTH_COLD -> "Cold"
                    BatteryManager.BATTERY_HEALTH_DEAD -> "Dead"
                    BatteryManager.BATTERY_HEALTH_GOOD -> "Good"
                    BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE -> "Over-Voltage"
                    BatteryManager.BATTERY_HEALTH_OVERHEAT -> "Overheat"
                    BatteryManager.BATTERY_HEALTH_UNKNOWN -> "Unknown"
                    BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE -> "Unspecified Failure"
                    else -> "No Data"
                }

                val bm: BatteryManager =
                    context.getSystemService(AppCompatActivity.BATTERY_SERVICE) as BatteryManager
                val batteryTotalCapacity =
                    bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER)
                var batteryCurrentNow =
                    bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW)
                var batteryCurrentAvg =
                    bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE)

                if (batteryCurrentNow > 3000) {
                    batteryCurrentNow /= 1000
                }

                if (batteryCurrentAvg == Int.MIN_VALUE) {
                    batteryCurrentAvg = 0
                }

                addBatteryUsage(
                    Date(),
                    batteryHealth,
                    battPowerSource,
                    batteryStatus,
                    volt,
                    (temp / 10).toFloat(),
                    batteryTotalCapacity,
                    batteryCurrentNow,
                    batteryCurrentAvg,
                    battLevel
                )
            }

            //Do whatever with the data here
        } catch (e: Exception) {
            //Log.v(TAG, "Battery Info Error")
        }
    }

    private fun addBatteryUsage(
        date: Date,
        health: String?,
        powerSource: String?,
        status: String?,
        voltage: Float,
        temp: Float,
        batteryCapacity: Int,
        currentNow: Int,
        currentAvg: Int,
        remainingCapacity: Int
    ) {
        GlobalScope.launch {
            addBatteryUsageUseCase(
                date,
                health,
                powerSource,
                status,
                voltage,
                temp,
                batteryCapacity,
                currentNow,
                currentAvg,
                remainingCapacity
            )
                .onSuccess {
                    //if ()
                    //showToast(context, context.getString(R.string.file_downloaded, it))
                }
                .onFailure {
                    //showToast(context, it.throwable.message ?: "Error")
                }
        }
    }
}