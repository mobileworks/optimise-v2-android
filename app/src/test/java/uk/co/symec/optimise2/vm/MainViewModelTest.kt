package uk.co.symec.optimise2.vm

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.work.WorkManager
import androidx.work.testing.WorkManagerTestInitHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import uk.co.symec.optimise2.data.utils.Connectivity
import uk.co.symec.optimise2.domain.model.BatteryInfoModel
import uk.co.symec.optimise2.domain.model.Success
import uk.co.symec.optimise2.domain.usecase.GetBatteryHealthUseCase
import uk.co.symec.optimise2.domain.usecase.GetBatteryInfoUseCase
import uk.co.symec.optimise2.domain.usecase.GetDailyScreenOnTimeUseCase
import uk.co.symec.optimise2.domain.usecase.GetLastTestResultUseCase

@ExperimentalCoroutinesApi
@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
class MainViewModelTest {
    private val getBatteryInfoUseCase: GetBatteryInfoUseCase =
        mock(GetBatteryInfoUseCase::class.java)
    private val getLastTestResultUseCase: GetLastTestResultUseCase =
        mock(GetLastTestResultUseCase::class.java)
    private val getBatteryHealthUseCase: GetBatteryHealthUseCase =
        mock(GetBatteryHealthUseCase::class.java)
    private val getDailyScreenOnTimeUseCase: GetDailyScreenOnTimeUseCase =
        mock(GetDailyScreenOnTimeUseCase::class.java)

    private val viewModel by lazy {
        MainViewModel(
            getBatteryInfoUseCase,
            getLastTestResultUseCase,
            getBatteryHealthUseCase,
            getDailyScreenOnTimeUseCase
        )
    }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        viewModel.connectivity = mock(Connectivity::class.java)
        WorkManagerTestInitHelper.initializeTestWorkManager(context)
        viewModel.workManager = WorkManager.getInstance(context)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test fdsf dsf sdf sdf `() = runBlocking {
        `when`(
            getBatteryInfoUseCase()
        ).thenReturn(Success(BatteryInfoModel.getEmptyModel()))

        viewModel.getBatteryInfo()
        assert(viewModel.viewState.value is uk.co.symec.optimise2.ui.base.Success)
    }
}