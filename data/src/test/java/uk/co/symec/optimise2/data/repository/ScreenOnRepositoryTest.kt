package uk.co.symec.optimise2.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import uk.co.symec.optimise2.data.db.dao.ScreenOnStatsDAO
import uk.co.symec.optimise2.data.utils.*
import java.util.*

class ScreenOnRepositoryTest {

    //private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val screenOnStatsDAO: ScreenOnStatsDAO = mock(ScreenOnStatsDAO::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider =
        spy(TestCoroutineContextProvider::class.java)

    private val screenOnRepository = ScreenOnRepositoryImpl(screenOnStatsDAO)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        screenOnRepository.connectivity = connectivity
        screenOnRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test write data into screen on db with success`() {
        runBlocking {
            val entity = fakeScreenOnEntity
            whenever(screenOnStatsDAO.insertData(entity)).thenReturn(
                fakeScreenOnEntity.date.time
            )
            screenOnRepository.addScreenOn(
                DATE, true, true
            )
            verify(screenOnStatsDAO, times(1)).insertData(entity)
        }
    }

    @Test
    fun `test write data into screen on db with no success`() {
        runBlocking {
            val entity = fakeScreenOnEntityEmptyDate
            whenever(screenOnStatsDAO.insertData(entity)).thenReturn(
                fakeScreenOnEntityEmptyDate.date.time
            )
            screenOnRepository.addScreenOn(
                Date(0), true, true
            )
            verify(screenOnStatsDAO, times(1)).insertData(entity)
        }
    }

    @Test
    fun `test get data from screen on db with success`() {
        runBlocking {
            val date = DATE
            val dateStart = screenOnRepository.setStartDate(date, -1)
            val dateEnd = screenOnRepository.setEndDate(date, -1)

            whenever(screenOnStatsDAO.getLastDateOfRange(dateStart.time, dateEnd.time)).thenReturn(
                fakeScreenOnEntity
            )
            screenOnRepository.getDailyScreenOnTime(
                date
            )
            verify(screenOnStatsDAO, times(1)).getLastDateOfRange(dateStart.time, dateEnd.time)
        }
    }
}