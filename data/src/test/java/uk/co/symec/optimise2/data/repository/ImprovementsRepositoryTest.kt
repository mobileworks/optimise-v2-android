package uk.co.symec.optimise2.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import uk.co.symec.optimise2.data.db.dao.ScreenOnStatsDAO
import uk.co.symec.optimise2.data.db.dao.TestResultDAO
import uk.co.symec.optimise2.data.utils.*
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.domain.model.TestResultModel
import uk.co.symec.optimise2.domain.repository.TestResultRepository
import java.util.*

class ImprovementsRepositoryTest {

    //private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider =
        spy(TestCoroutineContextProvider::class.java)

    private val improvementsRepository = ImprovementsRepositoryImpl()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        improvementsRepository.connectivity = connectivity
        improvementsRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test get last data from test result db with success`() {
        runBlocking {
            val entity = fakeTestResultEntity
            //whenever(testResultDAO.getLatestData()).thenReturn(entity)
            improvementsRepository.getImprovements(ImprovementType.ALERT)
            //verify(testResultDAO, times(1)).getLatestData()
        }
    }

    @Test
    fun `test get last data from test result db with no success`() {
        runBlocking {
            improvementsRepository.getImprovements(ImprovementType.ALERT)
//            whenever(testResultDAO.getLatestData()).thenReturn(null)
//            testResultRepository.getLastTestResult()
//            verify(testResultDAO, times(1)).getLatestData()
        }
    }
}