package uk.co.symec.optimise2.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import uk.co.symec.optimise2.data.db.dao.ScreenOnStatsDAO
import uk.co.symec.optimise2.data.db.dao.TestResultDAO
import uk.co.symec.optimise2.data.utils.*
import uk.co.symec.optimise2.domain.model.TestResultModel
import uk.co.symec.optimise2.domain.repository.TestResultRepository
import java.util.*

class TestResultRepositoryTest {

    //private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val testResultDAO: TestResultDAO = mock(TestResultDAO::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider =
        spy(TestCoroutineContextProvider::class.java)

    private val testResultRepository = TestResultRepositoryImpl(testResultDAO)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        testResultRepository.connectivity = connectivity
        testResultRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test write data into test result db with success`() {
        runBlocking {
            val entity = fakeTestResultEntity
            whenever(testResultDAO.insertData(entity)).thenReturn(
                fakeTestResultEntity.date.time
            )
            testResultRepository.addTestResult(
                TestResultModel.getEmptyModel()
            )
            verify(testResultDAO, times(1)).insertData(entity)
        }
    }

    @Test
    fun `test write data into test result db with no success`() {
        runBlocking {
            val entity = fakeTestResultEntityEmptyDate
            whenever(testResultDAO.insertData(entity)).thenReturn(
                fakeTestResultEntityEmptyDate.date.time
            )
            testResultRepository.addTestResult(
                TestResultModel(
                    Date(0), 0.0,0.0, true, 0, 0, 0, 0
                )
            )
            verify(testResultDAO, times(1)).insertData(entity)
        }
    }

    @Test
    fun `test get last data from test result db with success`() {
        runBlocking {
            val entity = fakeTestResultEntity
            whenever(testResultDAO.getLatestData()).thenReturn(entity)
            testResultRepository.getLastTestResult()
            verify(testResultDAO, times(1)).getLatestData()
        }
    }

    @Test
    fun `test get last data from test result db with no success`() {
        runBlocking {
            whenever(testResultDAO.getLatestData()).thenReturn(null)
            testResultRepository.getLastTestResult()
            verify(testResultDAO, times(1)).getLatestData()
        }
    }
}