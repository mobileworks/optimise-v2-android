package uk.co.symec.optimise2.data.repository

import android.content.Context
import org.junit.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import uk.co.symec.optimise2.data.db.dao.BatteryUsageDAO
import uk.co.symec.optimise2.data.utils.*
import uk.co.symec.optimise2.domain.model.BatteryInfoModel
import uk.co.symec.optimise2.domain.model.TimeLeftModel
import java.util.*

class BatteryUsageRepositoryTest {

    //private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val context: Context = spy(Context::class.java)
    private val batteryUsageDAO: BatteryUsageDAO = mock(BatteryUsageDAO::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider =
        spy(TestCoroutineContextProvider::class.java)

    private val batteryUsageRepository = BatteryUsageRepositoryImpl(
        batteryUsageDAO, context
    )

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        batteryUsageRepository.connectivity = connectivity
        batteryUsageRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test write data into battery usage db with success`() {
        runBlocking {
            val entity = fakeBatteryUsageEntity
            whenever(batteryUsageDAO.insertData(entity)).thenReturn(
                fakeBatteryUsageEntity.date.time
            )
            batteryUsageRepository.addBatteryUsage(
                DATE,
                HEALTH, POWER_SOURCE,
                STATUS, VOLTAGE, TEMP, BATTERY_CAPACITY, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY
            )

            verify(batteryUsageDAO, times(1)).insertData(entity)
        }
    }

    @Test
    fun `test write data into battery usage db with no success`() {
        runBlocking {
            val entity = fakeBatteryUsageEntityEmptyDate
            whenever(batteryUsageDAO.insertData(entity)).thenReturn(
                fakeBatteryUsageEntityEmptyDate.date.time
            )
            batteryUsageRepository.addBatteryUsage(
                Date(0),
                HEALTH, POWER_SOURCE,
                STATUS, VOLTAGE, TEMP, BATTERY_CAPACITY, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY
            )

            verify(batteryUsageDAO, times(1)).insertData(entity)
        }
    }

    @Test
    fun `test get data from battery usage db with success`() {
        runBlocking {
            whenever(batteryUsageDAO.getAllData()).thenReturn(
                fakeBatteryUsageEntityList2
            )
            batteryUsageRepository.getBatteryInfo()

            verify(batteryUsageDAO, times(1)).getAllData()
        }
    }

    @Test
    fun `test get data history from battery usage db with success`() {
        runBlocking {
            whenever(batteryUsageDAO.getLatestFullChargeValue()).thenReturn(
                1000
            )
            batteryUsageRepository.getBatteryHealth()

            verify(batteryUsageDAO, times(1)).getLatestFullChargeValue()
        }
    }

    @Test
    fun `test fun for converting time to hours and minutes 1`() {
        runBlocking {
            val result = batteryUsageRepository.getHoursAndMinutes(100)
            assertEquals(result, TimeLeftModel(1, 40))
        }
    }

    @Test
    fun `test fun for converting time to hours and minutes 2`() {
        runBlocking {
            val result = batteryUsageRepository.getHoursAndMinutes(0)
            assertEquals(result, TimeLeftModel(0, 0))
        }
    }

    @Test
    fun `test fun for converting time to hours and minutes 3`() {
        runBlocking {
            val result = batteryUsageRepository.getHoursAndMinutes(60)
            assertEquals(result, TimeLeftModel(1, 0))
        }
    }

    @Test
    fun `test fun for getting time battery left empty list`() {
        runBlocking {
            val list = fakeBatteryUsageEntityListEmpty
            val result = batteryUsageRepository.computeDischargeTime(list)
            assertEquals(result, BatteryInfoModel.getEmptyModel())
        }
    }

    @Test
    fun `test fun for getting time battery left one element`() {
        runBlocking {
            val list = fakeBatteryUsageEntityList
            val result = batteryUsageRepository.computeDischargeTime(list)
            assertEquals(result, BatteryInfoModel(POWER_SOURCE, STATUS, REMAINING_CAPACITY, 0, 0))        }
    }

    @Test
    fun `test fun for getting time battery left 2`() {
        runBlocking {
            val list2 = fakeBatteryUsageEntityList2
            val result = batteryUsageRepository.computeDischargeTime(list2)
            assertEquals(result, BatteryInfoModel(POWER_SOURCE, STATUS, REMAINING_CAPACITY, 0, 50))
        }
    }
}