package uk.co.symec.optimise2.data.utils

import uk.co.symec.optimise2.data.db.entity.BatteryUsageEntity
import uk.co.symec.optimise2.data.db.entity.ScreenOnStatsEntity
import uk.co.symec.optimise2.data.db.entity.TestResultEntity
import java.util.*

const val REMAINING_CAPACITY = 1000
const val DEVICE_ID = "1234"
const val CURRENT_AVG = 300
const val CURRENT_NOW = 300
const val BATTERY_CAPACITY = 3000
const val TEMP = 32.4f
const val VOLTAGE = 4.12f
const val STATUS = "Discharging"
const val POWER_SOURCE = "USB"
const val HEALTH = "good"
const val LONGITUDE = 0.0
const val LATITUDE = 0.0

val DATE = Date(1625742919)

const val FAKE_FAILURE_ERROR_CODE = 400

val fakeScreenOnEntity = ScreenOnStatsEntity(
    DATE, true, true
)
val fakeScreenOnEntityEmptyDate = ScreenOnStatsEntity(
    Date(0), true, true
)

val fakeTestResultEntity = TestResultEntity(
    DATE, LONGITUDE, LATITUDE, true, 0, 0, 0, 0
)
val fakeTestResultEntityEmptyDate = TestResultEntity(
    Date(0), LONGITUDE, LATITUDE, true, 0, 0, 0, 0
)

val fakeBatteryUsageEntity = BatteryUsageEntity(
    DATE, HEALTH, POWER_SOURCE,
    STATUS, VOLTAGE, TEMP, BATTERY_CAPACITY, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY
)

val fakeBatteryUsageEntity2 = BatteryUsageEntity(
    Date(1625741919), HEALTH, POWER_SOURCE,
    STATUS, VOLTAGE, TEMP, 3001, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY
)
val fakeBatteryUsageEntityEmptyDate = BatteryUsageEntity(
    Date(0), HEALTH, POWER_SOURCE,
    STATUS, VOLTAGE, TEMP, BATTERY_CAPACITY, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY
)
val fakeBatteryUsageEntityListEmpty = mutableListOf<BatteryUsageEntity>()
val fakeBatteryUsageEntityList = mutableListOf(fakeBatteryUsageEntity)
val fakeBatteryUsageEntityList2 = mutableListOf(fakeBatteryUsageEntity, fakeBatteryUsageEntity2)
