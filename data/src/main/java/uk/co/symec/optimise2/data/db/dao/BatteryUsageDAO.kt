package uk.co.symec.optimise2.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uk.co.symec.optimise2.data.db.entity.BatteryUsageEntity

@Dao
interface BatteryUsageDAO {

    @Query("SELECT * FROM battery_usage ORDER BY date DESC LIMIT 1")
    fun getLatestData(): BatteryUsageEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: BatteryUsageEntity): Long

    @Query("SELECT * FROM battery_usage ORDER BY date DESC")
    fun getAllData(): List<BatteryUsageEntity>?

    @Query("SELECT battery_capacity FROM battery_usage WHERE status = \"Full\" ORDER BY date DESC LIMIT 1")
    fun getLatestFullChargeValue(): Int?
}