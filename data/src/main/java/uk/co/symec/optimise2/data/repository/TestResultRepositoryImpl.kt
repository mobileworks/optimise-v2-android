package uk.co.symec.optimise2.data.repository

import kotlinx.coroutines.withContext
import uk.co.symec.optimise2.data.db.DB_ENTRY_ERROR
import uk.co.symec.optimise2.data.db.DB_INSERT_ERROR
import uk.co.symec.optimise2.data.db.dao.TestResultDAO
import uk.co.symec.optimise2.data.db.entity.TestResultEntity
import uk.co.symec.optimise2.domain.model.*
import uk.co.symec.optimise2.domain.repository.TestResultRepository
import javax.inject.Inject

class TestResultRepositoryImpl @Inject constructor(
    private val dao: TestResultDAO
) : BaseRepository(),
    TestResultRepository {
    override suspend fun addTestResult(result: TestResultModel): Result<Boolean> {
        return try {
            val entity = TestResultEntity(result.date, result.longitude, result.latitude, result.isScreenOn, result.freeStorage, result.totalStorage, result.freeMemory, result.totalMemory)
            if (dao.insertData(entity) > 0) {
                Success(true)
            } else {
                Success(false)
            }
        } catch (e: Exception) {
            Failure(HttpError(Throwable(DB_INSERT_ERROR)))
        }
    }

    override suspend fun getLastTestResult(): Result<TestResultModel> {
        return withContext(contextProvider.io) {
            dao.getLatestData()?.let {
                return@withContext Success(it.mapToDomainModel())
            }
            return@withContext Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
        }
    }
}

