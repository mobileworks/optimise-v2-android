package uk.co.symec.optimise2.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.squareup.moshi.Moshi
import uk.co.symec.optimise2.data.db.dao.BatteryUsageDAO
import uk.co.symec.optimise2.data.db.dao.ScreenOnStatsDAO
import uk.co.symec.optimise2.data.db.dao.TestResultDAO
import uk.co.symec.optimise2.data.db.entity.BatteryUsageEntity
import uk.co.symec.optimise2.data.db.entity.ScreenOnStatsEntity
import uk.co.symec.optimise2.data.db.entity.TestResultEntity

/**
 * The Room database for this app
 */

const val DB_ENTRY_ERROR = "No entry found in database"
const val DB_INSERT_ERROR = "Wrong insert into database"

@Database(entities = [BatteryUsageEntity::class, ScreenOnStatsEntity::class, TestResultEntity::class], version = 1, exportSchema = true)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun batteryUsageDao(): BatteryUsageDAO
    abstract fun screenOnStatsDao(): ScreenOnStatsDAO
    abstract fun gpsStatsDao(): TestResultDAO

    companion object {
        @Volatile var moshi: Moshi? = null
    }
}