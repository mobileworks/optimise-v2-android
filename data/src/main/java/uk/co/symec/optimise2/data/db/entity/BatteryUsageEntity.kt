package uk.co.symec.optimise2.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import uk.co.symec.optimise2.data.apiservice.DomainMapper
import uk.co.symec.optimise2.domain.model.BatteryUsageModel
import java.util.*

@Entity(
    tableName = "battery_usage"
)
data class BatteryUsageEntity(
    @PrimaryKey @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "health") val health: String?,
    @ColumnInfo(name = "power_source") val powerSource: String?,
    @ColumnInfo(name = "status") val status: String?,
    @ColumnInfo(name = "voltage") val voltage: Float,
    @ColumnInfo(name = "temp") val temp: Float,
    @ColumnInfo(name = "battery_capacity") var batteryCapacity: Int,
    @ColumnInfo(name = "current_now") var currentNow: Int,
    @ColumnInfo(name = "current_avg") var currentAvg: Int,
    @ColumnInfo(name = "remaining_capacity") var remainingCapacity: Int
) : DomainMapper<BatteryUsageModel> {
    override fun mapToDomainModel() = BatteryUsageModel(
        date,
        health,
        powerSource,
        status,
        voltage,
        temp,
        batteryCapacity,
        currentNow,
        currentAvg,
        remainingCapacity
    )
}