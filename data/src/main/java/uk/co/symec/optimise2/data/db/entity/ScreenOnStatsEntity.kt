package uk.co.symec.optimise2.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import uk.co.symec.optimise2.data.apiservice.DomainMapper
import uk.co.symec.optimise2.domain.model.ScreenOnStatsModel
import java.util.*

@Entity(
    tableName = "screen_on_stats"
)
data class ScreenOnStatsEntity(
    @PrimaryKey @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "screen_on") var screenOn: Boolean,
    @ColumnInfo(name = "in_foreground") var inForeground: Boolean
) : DomainMapper<ScreenOnStatsModel> {
    override fun mapToDomainModel() = ScreenOnStatsModel(
        date,
        screenOn,
        inForeground
    )
}