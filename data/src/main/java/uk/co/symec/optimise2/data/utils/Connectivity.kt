package uk.co.symec.optimise2.data.utils

interface Connectivity {
  fun hasNetworkAccess(): Boolean
}