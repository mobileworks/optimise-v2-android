package uk.co.symec.optimise2.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uk.co.symec.optimise2.data.repository.*
import uk.co.symec.optimise2.data.utils.Connectivity
import uk.co.symec.optimise2.data.utils.ConnectivityImpl
import uk.co.symec.optimise2.domain.repository.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideScreenOnRepository(repository: ScreenOnRepositoryImpl): ScreenOnRepository = repository

    @Provides
    @Singleton
    fun provideBatteryUsageRepository(repository: BatteryUsageRepositoryImpl): BatteryUsageRepository = repository

    @Provides
    @Singleton
    fun provideTestResultRepository(repository: TestResultRepositoryImpl): TestResultRepository = repository

    @Provides
    @Singleton
    fun provideStorageStateRepository(repository: StorageStatsRepositoryImpl): StorageStatsRepository = repository

    @Provides
    @Singleton
    fun provideMemoryRepository(repository: MemoryStatsRepositoryImpl): MemoryStatsRepository = repository

    @Provides
    @Singleton
    fun provideImprovementsRepository(repository: ImprovementsRepositoryImpl): ImprovementsRepository = repository

    @Provides
    @Singleton
    fun provideConnectivity(connectivity: ConnectivityImpl): Connectivity = connectivity
}