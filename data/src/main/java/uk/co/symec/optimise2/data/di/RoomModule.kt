package uk.co.symec.optimise2.data.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uk.co.symec.optimise2.data.db.AppDatabase
import uk.co.symec.optimise2.data.db.dao.BatteryUsageDAO
import uk.co.symec.optimise2.data.db.dao.ScreenOnStatsDAO
import uk.co.symec.optimise2.data.db.dao.TestResultDAO
import uk.co.symec.optimise2.data.utils.Constants
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            Constants.DATABASE_NAME)
            //.addMigrations(MIGRATION_1_2, MIGRATION_2_3)
            .build()
            .apply { AppDatabase.moshi = NetworkModule.moshi }
    }

    @Provides
    fun provideBatteryUsageDao(appDatabase: AppDatabase): BatteryUsageDAO {
        return appDatabase.batteryUsageDao()
    }

    @Provides
    fun provideScreenOnStatsDao(appDatabase: AppDatabase): ScreenOnStatsDAO {
        return appDatabase.screenOnStatsDao()
    }

    @Provides
    fun provideTestResultDao(appDatabase: AppDatabase): TestResultDAO {
        return appDatabase.gpsStatsDao()
    }

//    val MIGRATION_1_2 = object : Migration(1, 2) {
//        override fun migrate(database: SupportSQLiteDatabase) {
//            database.execSQL(
//                """ALTER TABLE media ADD COLUMN score INTEGER NOT NULL DEFAULT 0
//                """.trimIndent()
//            )
//
//            database.execSQL(
//                """ALTER TABLE content ADD COLUMN signature TEXT
//                """.trimIndent()
//            )
//
//            database.execSQL(
//                """ALTER TABLE content ADD COLUMN confirmation_required INTEGER NOT NULL DEFAULT TRUE
//                """.trimIndent()
//            )
//        }
//    }
//
//    val MIGRATION_2_3 = object : Migration(2, 3) {
//        override fun migrate(database: SupportSQLiteDatabase) {
//            database.execSQL(
//                """ALTER TABLE media ADD COLUMN seen_at INTEGER NULL DEFAULT NULL
//                """.trimIndent()
//            )
//        }
//    }
}