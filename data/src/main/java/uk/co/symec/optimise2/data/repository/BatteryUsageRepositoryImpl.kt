package uk.co.symec.optimise2.data.repository

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.withContext
import uk.co.symec.optimise2.data.db.DB_ENTRY_ERROR
import uk.co.symec.optimise2.data.db.DB_INSERT_ERROR
import uk.co.symec.optimise2.data.db.dao.BatteryUsageDAO
import uk.co.symec.optimise2.data.db.entity.BatteryUsageEntity
import uk.co.symec.optimise2.domain.model.*
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository
import java.util.*
import javax.inject.Inject

class BatteryUsageRepositoryImpl @Inject constructor(
    private val dao: BatteryUsageDAO,
    @ApplicationContext private val context: Context
) : BaseRepository(),
    BatteryUsageRepository {
    override suspend fun addBatteryUsage(
        date: Date,
        health: String?,
        powerSource: String?,
        status: String?,
        voltage: Float,
        temp: Float,
        batteryCapacity: Int,
        currentNow: Int,
        currentAvg: Int,
        remainingCapacity: Int
    ): Result<Boolean> {
        return try {
            val result = dao.insertData(
                BatteryUsageEntity(
                    date,
                    health,
                    powerSource,
                    status,
                    voltage,
                    temp,
                    batteryCapacity,
                    currentNow,
                    currentAvg,
                    remainingCapacity
                )
            )
            if (result > 0) {
                Success(true)
            } else {
                Success(false)
            }
        } catch (e: Exception) {
            Failure(HttpError(Throwable(DB_INSERT_ERROR)))
        }
    }

    override suspend fun getBatteryInfo(): Result<BatteryInfoModel> {
        return withContext(contextProvider.io) {
            dao.getAllData()?.let {
                return@withContext if (it.isNotEmpty()) {
                    Success(computeDischargeTime(it))
                } else {
                    Success(BatteryInfoModel.getEmptyModel())
                }
            }
            return@withContext Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
        }
    }

    override suspend fun getBatteryHealth(): Result<Float> {
        return withContext(contextProvider.io) {
            dao.getLatestFullChargeValue()?.let {
                val initialLevel = getBatteryDesignCapacity(context)
                if (initialLevel > 0) {
                    val compute = ((it / 1000) / initialLevel.toFloat()) * 100
                    return@withContext Success(compute)
                } else {
                    return@withContext Success(1f)
                }
            }
            return@withContext Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
        }
    }

    private fun getBatteryDesignCapacity(context: Context?): Int {
        val mPowerProfile: Any
        var batteryCapacity = 0.0
        val POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile"
        try {
            mPowerProfile = Class.forName(POWER_PROFILE_CLASS)
                .getConstructor(Context::class.java)
                .newInstance(context)
            batteryCapacity = Class
                .forName(POWER_PROFILE_CLASS)
                .getMethod("getBatteryCapacity")
                .invoke(mPowerProfile) as Double
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return batteryCapacity.toInt()
    }

    fun getHoursAndMinutes(time: Int): TimeLeftModel {
        var minutes = time
        val hours: Int = minutes / 60
        minutes -= hours * 60
        return TimeLeftModel(hours, minutes)
    }

    fun computeDischargeTime(it: List<BatteryUsageEntity>): BatteryInfoModel {
        val charging = -1
        var chargeStatus: String? = null
        var powerSource: String? = null
        var remainingCapacity = 0
        var batteryLevel = 0

        it.firstOrNull()?.let { entity ->
            entity.status?.let {
                chargeStatus = it
            }
            remainingCapacity = entity.batteryCapacity
            powerSource = entity.powerSource
            batteryLevel = entity.remainingCapacity
        }

        var previousUsage: BatteryUsageEntity? = null

        val dischargeSamples = ArrayList<Double>()
        for (currentUsage in it) {
            if (previousUsage == null) {
                previousUsage = currentUsage
                continue
            }

            val currentCapacity: Int = currentUsage.batteryCapacity
            val previousCapacity: Int = previousUsage.batteryCapacity
            val discharge: Double = charging.toDouble() * (previousCapacity - currentCapacity)

            // prevent division by zero OR a negative charge/discharge:
            // i.e., only positive differences are considered
            if (discharge <= 0.0) {
                previousUsage = currentUsage
                continue
            }
            // in seconds
            // in seconds
            val elapsedTime: Long = kotlin.math.abs(
                (currentUsage.date.time - previousUsage.date.time) / 1000
            )
            dischargeSamples.add(elapsedTime / discharge)

            previousUsage = currentUsage
        }

        if (dischargeSamples.size == 0) {
            return BatteryInfoModel(powerSource, chargeStatus, batteryLevel, 0, 0)
        }

        var sumDischarges = 0.0
        for (discharge in dischargeSamples) {
            sumDischarges += discharge
        }
        val dischargeRatio = sumDischarges / dischargeSamples.size

        val result = ((remainingCapacity.toDouble() * dischargeRatio) / 60).toInt()

        val time = getHoursAndMinutes(result)

        return BatteryInfoModel(powerSource, chargeStatus, batteryLevel, time.hours, time.minutes)
    }
}

