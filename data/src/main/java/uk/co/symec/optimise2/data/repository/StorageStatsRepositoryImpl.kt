package uk.co.symec.optimise2.data.repository

import android.content.Context
import android.os.StatFs
import dagger.hilt.android.qualifiers.ApplicationContext
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.StorageStatsModel
import uk.co.symec.optimise2.domain.model.Success
import uk.co.symec.optimise2.domain.repository.StorageStatsRepository
import java.util.*
import javax.inject.Inject

class StorageStatsRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : BaseRepository(),
    StorageStatsRepository {

    private val KB = 1024
    private val MB = KB * 1024
    override suspend fun getStorageStats(): Result<StorageStatsModel> {
        val stat = StatFs(context.filesDir.path)
        val total = (stat.totalBytes/MB).toInt()
        val free = (stat.availableBytes/MB).toInt()

        return Success(StorageStatsModel(Date(), total, free))
    }
}

