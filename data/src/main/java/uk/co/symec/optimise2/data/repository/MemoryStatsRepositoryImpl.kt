package uk.co.symec.optimise2.data.repository

import android.app.ActivityManager
import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import uk.co.symec.optimise2.domain.model.MemoryStatsModel
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.Success
import uk.co.symec.optimise2.domain.repository.MemoryStatsRepository
import java.util.*
import javax.inject.Inject

class MemoryStatsRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : BaseRepository(),
    MemoryStatsRepository {

    private val KB = 1024
    private val MB = KB * 1024
    override suspend fun getMemoryStats(): Result<MemoryStatsModel> {
        (context.applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager)?.let {
            activityManager ->
            val memInfo = ActivityManager.MemoryInfo()
            activityManager.getMemoryInfo(memInfo)

            val totalMemory = memInfo.totalMem/MB
            val freeMemory = memInfo.availMem/MB
            return Success(MemoryStatsModel(Date(), totalMemory.toInt(), freeMemory.toInt()))

            //return Success(MemoryStatsModel(Date(), 100, 11))
        }

        return Success(MemoryStatsModel(Date(), 0, 0))
    }
}

