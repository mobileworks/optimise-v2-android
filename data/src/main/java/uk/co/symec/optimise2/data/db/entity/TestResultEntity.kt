package uk.co.symec.optimise2.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import uk.co.symec.optimise2.data.apiservice.DomainMapper
import uk.co.symec.optimise2.domain.model.TestResultModel
import java.util.*

@Entity(
    tableName = "test_result"
)
data class TestResultEntity(
    @PrimaryKey @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "longitude") var longitude: Double,
    @ColumnInfo(name = "latitude") var latitude: Double,
    @ColumnInfo(name = "is_screen_on") var isScreenOn: Boolean,
    @ColumnInfo(name = "free_space") var freeSpace: Int,
    @ColumnInfo(name = "total_space") var totalSpace: Int,
    @ColumnInfo(name = "free_memory") var freeMemory: Int,
    @ColumnInfo(name = "total_memory") var totalMemory: Int
) : DomainMapper<TestResultModel> {
    override fun mapToDomainModel() = TestResultModel(
        date,
        longitude,
        latitude,
        isScreenOn,
        freeSpace,
        totalSpace,
        freeMemory,
        totalMemory
    )
}