package uk.co.symec.optimise2.data.repository

import kotlinx.coroutines.withContext
import uk.co.symec.optimise2.data.db.DB_INSERT_ERROR
import uk.co.symec.optimise2.data.db.dao.ScreenOnStatsDAO
import uk.co.symec.optimise2.data.db.entity.ScreenOnStatsEntity
import uk.co.symec.optimise2.domain.model.Failure
import uk.co.symec.optimise2.domain.model.HttpError
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.Success
import uk.co.symec.optimise2.domain.repository.ScreenOnRepository
import java.util.*
import javax.inject.Inject

class ScreenOnRepositoryImpl @Inject constructor(
    private val dao: ScreenOnStatsDAO
) : BaseRepository(),
    ScreenOnRepository {
    override suspend fun addScreenOn(
        date: Date,
        screenOn: Boolean,
        isForeground: Boolean
    ): Result<Boolean> {
        return try {
            if (dao.insertData(ScreenOnStatsEntity(date, screenOn, isForeground)) > 0) {
                Success(true)
            } else {
                Success(false)
            }
        } catch (e: Exception) {
            Failure(HttpError(Throwable(DB_INSERT_ERROR)))
        }
    }

    override suspend fun getDailyScreenOnTime(date: Date): Result<Int> {
        withContext(contextProvider.io) {
            val calStartPrev = setStartDate(date, -1)
            val calEndPrev = setEndDate(date, -1)
            var previous:Date? = null

            dao.getLastDateOfRange(calStartPrev.time, calEndPrev.time)?.let {
                if (it.screenOn) {
                    previous = it.date
                }
            }

            val calStart = setStartDate(date)
            val calEnd = setEndDate(date)

            var totalTimeOn = 0L
            dao.getAllDatesByRange(calStart.time, calEnd.time)?.let {

                it.forEach {
                    println(it.screenOn)

                    if (it.screenOn) {
                        previous = it.date
                    }

                    if (!it.screenOn && previous != null) {
                        val timeOn = (it.date.time - previous!!.time)
                        println("Time on ${timeOn.toFloat()/60000} min")
                        totalTimeOn += timeOn
                        previous = null
                    }
                }

                val result = totalTimeOn.toFloat()/60000
                return@withContext Success(result)
            }
        }
        return Success(0)
    }

    fun setStartDate(date: Date, dateChange: Int = 0): Calendar {
        val calStart = Calendar.getInstance()
        calStart.time = date
        if (dateChange != 0) {
            calStart.set(Calendar.DAY_OF_MONTH, calStart.get(Calendar.DAY_OF_MONTH) + dateChange)
        }
        calStart.set(Calendar.HOUR, 0)
        calStart.set(Calendar.HOUR_OF_DAY, 0)
        calStart.set(Calendar.MINUTE, 0)
        calStart.set(Calendar.SECOND, 0)
        calStart.set(Calendar.MILLISECOND, 0)
        return calStart
    }

    fun setEndDate(date: Date, dateChange: Int = 0): Calendar {
        val calEnd = Calendar.getInstance()
        calEnd.time = date
        if (dateChange != 0) {
            calEnd.set(Calendar.DAY_OF_MONTH, calEnd.get(Calendar.DAY_OF_MONTH) + dateChange)
        }
        calEnd.set(Calendar.HOUR, 23)
        calEnd.set(Calendar.HOUR_OF_DAY, 23)
        calEnd.set(Calendar.MINUTE, 59)
        calEnd.set(Calendar.SECOND, 59)
        calEnd.set(Calendar.MILLISECOND, 59)
        return calEnd
    }
}

