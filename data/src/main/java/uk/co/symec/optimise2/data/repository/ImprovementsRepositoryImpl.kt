package uk.co.symec.optimise2.data.repository

import uk.co.symec.optimise2.domain.model.ImprovementItemModel
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.Success
import uk.co.symec.optimise2.domain.repository.ImprovementsRepository
import javax.inject.Inject

class ImprovementsRepositoryImpl @Inject constructor() : BaseRepository(),
    ImprovementsRepository {
    override suspend fun getImprovements(improvementType: ImprovementType): Result<List<ImprovementItemModel>> {
        val list = mutableListOf<ImprovementItemModel>()
        return Success(list)
    }
}

