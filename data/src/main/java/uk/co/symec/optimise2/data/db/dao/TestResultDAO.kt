package uk.co.symec.optimise2.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uk.co.symec.optimise2.data.db.entity.TestResultEntity

@Dao
interface TestResultDAO {

    @Query("SELECT * FROM test_result ORDER BY date DESC LIMIT 1")
    fun getLatestData(): TestResultEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: TestResultEntity): Long

    @Query("SELECT * FROM test_result ORDER BY date DESC")
    fun getAllData(): List<TestResultEntity>?
}