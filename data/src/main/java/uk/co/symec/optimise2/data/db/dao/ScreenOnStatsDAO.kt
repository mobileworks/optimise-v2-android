package uk.co.symec.optimise2.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uk.co.symec.optimise2.data.db.entity.ScreenOnStatsEntity
import java.util.*

@Dao
interface ScreenOnStatsDAO {

    @Query("SELECT * FROM screen_on_stats ORDER BY date DESC LIMIT 1")
    fun getLatestData(): ScreenOnStatsEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: ScreenOnStatsEntity): Long

    @Query("SELECT * FROM screen_on_stats ORDER BY date DESC")
    fun getAllData(): List<ScreenOnStatsEntity>?

    @Query("SELECT * FROM screen_on_stats WHERE date BETWEEN :startDate AND :endDate ORDER BY date ASC")
    fun getAllDatesByRange(startDate: Date, endDate: Date): List<ScreenOnStatsEntity>?

    @Query("SELECT * FROM screen_on_stats WHERE date BETWEEN :startDate AND :endDate ORDER BY date DESC LIMIT 1")
    fun getLastDateOfRange(startDate: Date, endDate: Date): ScreenOnStatsEntity?

}