package uk.co.symec.optimise2.domain

import uk.co.symec.optimise2.domain.model.Result

interface BaseUseCase<T : Any, R: Any> {
  suspend operator fun invoke(param: T): Result<R>
}