package uk.co.symec.optimise2.domain.model

import java.util.*

const val minStrengthGsm = -130
const val maxStrengthGsm = -30
const val criticalLevelGsm = -120
const val warningLevelGsm = -105

data class MobileStatsModel(
    override var date: Date,
    override var isAvailable: Boolean,
    override var signalStrength: Int,
    override var type: String?,
    override var name: String?
): ConnectionStatsModel {

    override var status = 0

    override fun getPercentage(): Int {
        return when (signalStrength) {
            in minStrengthGsm..criticalLevelGsm -> {
                status = 2
                kotlin.math.abs((criticalLevelGsm - signalStrength) - 11) //1-10
            }
            in criticalLevelGsm..warningLevelGsm -> {
                status = 1
                kotlin.math.abs((warningLevelGsm - signalStrength) - 21) //2-20
            }
            in warningLevelGsm..maxStrengthGsm -> {
                status = 0
                val result = (((signalStrength - maxStrengthGsm) + 100))
                if (result >= 100) {
                    100
                } else {
                    result
                } //21-100
            }
            else -> {
                status = 1
                1
            }
        }
    }

    companion object {
        fun getEmptyModel(): MobileStatsModel {
            return MobileStatsModel(
                Date(), true, 0, "", ""
            )
        }

        fun getEmptyModelWithSignalStrength(signalStrength: Int): MobileStatsModel {
            return MobileStatsModel(
                Date(), true, signalStrength, "", ""
            )
        }
    }
}