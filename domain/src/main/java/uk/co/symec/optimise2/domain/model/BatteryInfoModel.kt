package uk.co.symec.optimise2.domain.model

data class BatteryInfoModel(
    val powerSource: String?,
    val status: String?,
    val batteryLevel: Int,
    val batteryTimeLeftHours: Int,
    val batteryTimeLeftMinutes: Int
) {
    companion object {
        fun getEmptyModel(): BatteryInfoModel {
            return BatteryInfoModel(
                null, null, 0, 0, 0
            )
        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}