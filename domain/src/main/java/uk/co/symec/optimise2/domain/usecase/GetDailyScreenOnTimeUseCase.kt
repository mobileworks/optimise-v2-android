package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import java.util.*

interface GetDailyScreenOnTimeUseCase {
    //time in minutes
    suspend operator fun invoke(date: Date): Result<Int>
}