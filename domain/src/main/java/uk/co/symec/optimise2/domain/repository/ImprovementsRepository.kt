package uk.co.symec.optimise2.domain.repository

import uk.co.symec.optimise2.domain.model.ImprovementItemModel
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.domain.model.Result

interface ImprovementsRepository {
    suspend fun getImprovements(improvementType: ImprovementType): Result<List<ImprovementItemModel>>
}