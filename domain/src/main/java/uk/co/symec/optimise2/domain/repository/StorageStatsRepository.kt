package uk.co.symec.optimise2.domain.repository

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.StorageStatsModel

interface StorageStatsRepository {
    suspend fun getStorageStats(): Result<StorageStatsModel>
}