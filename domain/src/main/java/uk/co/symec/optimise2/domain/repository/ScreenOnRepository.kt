package uk.co.symec.optimise2.domain.repository

import uk.co.symec.optimise2.domain.model.Result
import java.util.*

interface ScreenOnRepository {
    suspend fun addScreenOn(date: Date, screenOn: Boolean, isForeground: Boolean): Result<Boolean>

    suspend fun getDailyScreenOnTime(date: Date): Result<Int>

}