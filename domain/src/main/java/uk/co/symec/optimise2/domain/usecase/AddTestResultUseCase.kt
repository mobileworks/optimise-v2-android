package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.TestResultModel

interface AddTestResultUseCase {
    suspend operator fun invoke(result: TestResultModel): Result<Boolean>
}