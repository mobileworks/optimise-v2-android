package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.TestResultModel
import uk.co.symec.optimise2.domain.repository.TestResultRepository
import javax.inject.Inject

class GetLastTestResultUseCaseImpl @Inject constructor(private val repository: TestResultRepository) :
    GetLastTestResultUseCase {
    override suspend fun invoke(): Result<TestResultModel> {
        return repository.getLastTestResult()
    }
}