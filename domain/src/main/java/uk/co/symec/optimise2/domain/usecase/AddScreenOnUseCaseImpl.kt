package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.ScreenOnRepository
import java.util.*
import javax.inject.Inject

class AddScreenOnUseCaseImpl @Inject constructor(private val repository: ScreenOnRepository) :
    AddScreenOnUseCase {
    override suspend fun invoke(date: Date, screenOn: Boolean, isForeground: Boolean): Result<Boolean> {
        return repository.addScreenOn(date, screenOn, isForeground)
    }
}