package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import java.util.*

interface AddBatteryUsageUseCase {
    suspend operator fun invoke(
        date: Date, health: String?,
        powerSource: String?,
        status: String?,
        voltage: Float,
        temp: Float,
        batteryCapacity: Int,
        currentNow: Int,
        currentAvg: Int,
        remainingCapacity: Int): Result<Boolean>
}