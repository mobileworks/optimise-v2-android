package uk.co.symec.optimise2.domain.model

import java.util.*

interface ConnectionStatsModel {

    var date: Date
    var isAvailable: Boolean
    var signalStrength: Int
    var name: String?
    var type: String?

    var status: Int

    fun getPercentage(): Int
}