package uk.co.symec.optimise2.domain.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uk.co.symec.optimise2.domain.usecase.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Provides
    @Singleton
    fun provideAddScreenOnUseCase(addScreenOnUseCase: AddScreenOnUseCaseImpl): AddScreenOnUseCase = addScreenOnUseCase

    @Provides
    @Singleton
    fun provideAddBatteryUsageUseCase(addBatteryUsageUseCase: AddBatteryUsageUseCaseImpl): AddBatteryUsageUseCase = addBatteryUsageUseCase

    @Provides
    @Singleton
    fun provideAddTestResultUseCase(addTestResultUseCase: AddTestResultUseCaseImpl): AddTestResultUseCase = addTestResultUseCase

    @Provides
    @Singleton
    fun provideGetLastTestResultUseCase(getLastTestResultUseCase: GetLastTestResultUseCaseImpl): GetLastTestResultUseCase = getLastTestResultUseCase

    @Provides
    @Singleton
    fun provideGetStorageStatsUseCase(getStorageStatsUseCase: GetStorageStatsUseCaseImpl): GetStorageStatsUseCase = getStorageStatsUseCase

    @Provides
    @Singleton
    fun provideGetMemoryStatsUseCase(getMemoryStatsUseCase: GetMemoryStatsUseCaseImpl): GetMemoryStatsUseCase = getMemoryStatsUseCase

    @Provides
    @Singleton
    fun provideGetBatteryInfoUseCase(getBatteryInfoUseCase: GetBatteryInfoUseCaseImpl): GetBatteryInfoUseCase = getBatteryInfoUseCase

    @Provides
    @Singleton
    fun provideGetImprovementsListUseCase(getImprovementsList: GetImprovementsListUseCaseImpl): GetImprovementsListUseCase = getImprovementsList

    @Provides
    @Singleton
    fun provideGetBatteryHealthUseCase(getBatteryHealthUseCase: GetBatteryHealthUseCaseImpl): GetBatteryHealthUseCase = getBatteryHealthUseCase

    @Provides
    @Singleton
    fun provideGetDailyScreenOnTimeUseCase(getDailyScreenOnTimeUseCase: GetDailyScreenOnTimeUseCaseImpl): GetDailyScreenOnTimeUseCase = getDailyScreenOnTimeUseCase

}