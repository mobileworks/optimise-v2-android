package uk.co.symec.optimise2.domain.model

enum class ImprovementType {
    CRITICAL, ALERT
}

data class ImprovementItemModel(
    val type: ImprovementType,
    val title: String,
    val text: String,
    val label: String
) {
    companion object {
//        fun getEmptyModel(): ImprovementItemModel {
//            return ImprovementItemModel(0, 0)
//        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}