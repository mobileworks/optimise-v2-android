package uk.co.symec.optimise2.domain.repository

import uk.co.symec.optimise2.domain.model.BatteryInfoModel
import uk.co.symec.optimise2.domain.model.Result
import java.util.*

interface BatteryUsageRepository {
    suspend fun addBatteryUsage(date: Date,
                                health: String?,
                                powerSource: String?,
                                status: String?,
                                voltage: Float,
                                temp: Float,
                                batteryCapacity: Int,
                                currentNow: Int,
                                currentAvg: Int,
                                remainingCapacity: Int): Result<Boolean>

    suspend fun getBatteryInfo(): Result<BatteryInfoModel>

    suspend fun getBatteryHealth(): Result<Float>
}