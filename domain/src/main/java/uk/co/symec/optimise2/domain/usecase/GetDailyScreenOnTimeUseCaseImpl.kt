package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.ScreenOnRepository
import java.util.*
import javax.inject.Inject

class GetDailyScreenOnTimeUseCaseImpl @Inject constructor(private val repository: ScreenOnRepository) :
    GetDailyScreenOnTimeUseCase {
    override suspend fun invoke(date: Date): Result<Int> {
        return repository.getDailyScreenOnTime(date)
    }
}