package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.StorageStatsModel
import uk.co.symec.optimise2.domain.repository.StorageStatsRepository
import javax.inject.Inject

class GetStorageStatsUseCaseImpl @Inject constructor(private val repository: StorageStatsRepository) :
    GetStorageStatsUseCase {
    override suspend fun invoke(): Result<StorageStatsModel> {
        return repository.getStorageStats()
    }
}