package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.BatteryInfoModel
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository
import javax.inject.Inject

class GetBatteryInfoUseCaseImpl @Inject constructor(private val repository: BatteryUsageRepository) :
    GetBatteryInfoUseCase {
    override suspend fun invoke(): Result<BatteryInfoModel> {
        return repository.getBatteryInfo()
    }
}