package uk.co.symec.optimise2.domain.repository

import uk.co.symec.optimise2.domain.model.MemoryStatsModel
import uk.co.symec.optimise2.domain.model.Result

interface MemoryStatsRepository {
    suspend fun getMemoryStats(): Result<MemoryStatsModel>
}