package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.BatteryInfoModel
import uk.co.symec.optimise2.domain.model.Result

interface GetBatteryInfoUseCase {
    suspend operator fun invoke(): Result<BatteryInfoModel>
}