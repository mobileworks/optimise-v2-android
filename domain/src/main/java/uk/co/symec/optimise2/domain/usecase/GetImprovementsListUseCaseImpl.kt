package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.ImprovementItemModel
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.ImprovementsRepository
import javax.inject.Inject

class GetImprovementsListUseCaseImpl @Inject constructor(private val repository: ImprovementsRepository) :
    GetImprovementsListUseCase {
    override suspend fun invoke(improvementType: ImprovementType): Result<List<ImprovementItemModel>> {
        return repository.getImprovements(improvementType)
    }
}