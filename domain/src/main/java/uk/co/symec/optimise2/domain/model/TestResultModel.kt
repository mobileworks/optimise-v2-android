package uk.co.symec.optimise2.domain.model

import java.util.*

data class TestResultModel(
    var date: Date,
    var longitude: Double,
    var latitude: Double,
    var isScreenOn: Boolean,
    var freeStorage: Int,
    var totalStorage: Int,
    var freeMemory: Int,
    var totalMemory: Int
) {

    fun getStoragePercentage(): Int {
        return if (totalStorage > 0) {
            (((totalStorage - freeStorage.toFloat()) / totalStorage) * 100).toInt()
        } else {
            0
        }
    }

    fun getMemoryPercentage(): Int {
        return if (totalMemory > 0) {
            (((totalMemory - freeMemory.toFloat()) / totalMemory) * 100).toInt()
        } else {
            0
        }
    }

    companion object {
        fun getEmptyModel(): TestResultModel {
            return TestResultModel(
                Date(1625742919), 0.0, 0.0, true, 0, 0, 0, 0
            )
        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}