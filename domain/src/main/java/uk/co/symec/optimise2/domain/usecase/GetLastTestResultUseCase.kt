package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.TestResultModel

interface GetLastTestResultUseCase {
    suspend operator fun invoke(): Result<TestResultModel>
}