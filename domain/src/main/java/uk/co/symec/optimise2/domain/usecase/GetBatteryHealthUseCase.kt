package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result

interface GetBatteryHealthUseCase {
    suspend operator fun invoke(): Result<Float>
}