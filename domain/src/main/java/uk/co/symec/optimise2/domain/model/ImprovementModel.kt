package uk.co.symec.optimise2.domain.model

data class ImprovementModel(
    val noOfWarning: Int,
    val noOfCritical: Int,
    val storageStatus: Int,
    val ramStatus: Int,
    val mobileStatus: Int,
    val wifiStatus: Int
) {
    companion object {
        fun getEmptyModel(): ImprovementModel {
            return ImprovementModel(0, 0, 0, 0, 0, 0)
        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}