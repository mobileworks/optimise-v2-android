package uk.co.symec.optimise2.domain.model

import java.util.*

data class MemoryStatsModel(
    var date: Date,
    var total: Int,
    var free: Int
) {

    var status = 0

    fun getMemoryPercentage(): Int {
        return if (total > 0) {
            val result = (((total - free.toFloat()) / total) * 100).toInt()
            status = when (result) {
                in 81..90 -> 1
                in 91..100 -> 2
                else -> 0
            }
            result
        } else {
            0
        }
    }

    companion object {
        fun getEmptyModel(): MemoryStatsModel {
            return MemoryStatsModel(
                Date(), 0, 0
            )
        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}