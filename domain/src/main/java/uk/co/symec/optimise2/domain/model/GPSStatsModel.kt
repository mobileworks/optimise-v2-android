package uk.co.symec.optimise2.domain.model

import java.util.*

data class GPSStatsModel(
    var date: Date,
    var longitude: Double,
    var latitude: Double
) {
    companion object {
        const val emptyLatitude = -200.0
        const val emptyLongitude = -200.0
//        fun getEmptyModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "", "", "", false, false, Date(),
//                Date(), 0, null, false, mutableListOf()
//            )
//        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}