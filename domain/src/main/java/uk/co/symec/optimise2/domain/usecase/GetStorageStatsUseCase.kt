package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.StorageStatsModel

interface GetStorageStatsUseCase {
    suspend operator fun invoke(): Result<StorageStatsModel>
}