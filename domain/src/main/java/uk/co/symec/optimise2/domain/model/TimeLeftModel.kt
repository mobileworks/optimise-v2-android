package uk.co.symec.optimise2.domain.model

data class TimeLeftModel(
    val hours: Int,
    val minutes: Int
) {
    companion object {
        fun getEmptyModel(): TimeLeftModel {
            return TimeLeftModel(0, 0)
        }
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}