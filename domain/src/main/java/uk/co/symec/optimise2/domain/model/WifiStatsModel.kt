package uk.co.symec.optimise2.domain.model

import java.util.*

const val minStrength = -90
const val maxStrength = -30
const val criticalLevel = -81
const val warningLevel = -71

class WifiStatsModel(
    override var date: Date,
    override var isAvailable: Boolean,
    override var signalStrength: Int,
    override var name: String?,
    override var type: String?
) : ConnectionStatsModel {
    override var status = 0

    override fun getPercentage(): Int {
        return when (signalStrength) {
            in minStrength..criticalLevel -> {
                status = 2
                kotlin.math.abs((criticalLevel - signalStrength) - 10) //1-10
            }
            in criticalLevel..warningLevel -> {
                status = 1
                kotlin.math.abs((warningLevel - signalStrength) - 20) //2-20
            }
            in warningLevel..maxStrength -> {
                status = 0
                val result = (((signalStrength - maxStrength) + 41) * 2) + 19
                if (result >= 100) {
                    100
                } else {
                    result
                }//21-100
            }
            else -> {
                status = 0
                0
            }
        }
    }

    companion object {
        fun getEmptyModel(): WifiStatsModel {
            return WifiStatsModel(
                Date(), true, 0, "", ""
            )
        }

        fun getEmptyModelWithSignalStrength(signalStrength: Int): WifiStatsModel {
            return WifiStatsModel(
                Date(), true, signalStrength, "", ""
            )
        }
    }
}