package uk.co.symec.optimise2.domain.repository

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.TestResultModel

interface TestResultRepository {
    suspend fun addTestResult(result: TestResultModel): Result<Boolean>

    suspend fun getLastTestResult(): Result<TestResultModel>
}