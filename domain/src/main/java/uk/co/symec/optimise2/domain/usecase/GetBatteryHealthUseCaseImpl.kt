package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository
import javax.inject.Inject

class GetBatteryHealthUseCaseImpl @Inject constructor(private val repository: BatteryUsageRepository) :
    GetBatteryHealthUseCase {
    override suspend fun invoke(): Result<Float> {
        return repository.getBatteryHealth()
    }
}