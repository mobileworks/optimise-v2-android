package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.MemoryStatsModel
import uk.co.symec.optimise2.domain.model.Result

interface GetMemoryStatsUseCase {
    suspend operator fun invoke(): Result<MemoryStatsModel>
}