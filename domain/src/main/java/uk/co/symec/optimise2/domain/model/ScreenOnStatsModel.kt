package uk.co.symec.optimise2.domain.model

import java.text.SimpleDateFormat
import java.util.*

data class ScreenOnStatsModel(
    val date: Date,
    val screenOn: Boolean = false,
    val inForeGround: Boolean = false
) {
    val getParsedDate: String
        get() {
            val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
            return formatter.format(date)
        }

    val getTimePast: String
        get() {
            return when (val minuteDifference =
                (System.currentTimeMillis() - date.time) / 60000) {
                in 0..60 -> "$minuteDifference m"
                in 61..1440 -> {
                    val x = minuteDifference / 60
                    "$x h"
                }
                else -> {
                    val x = minuteDifference / 1440
                    "$x d"
                }
            }
        }

    companion object {
//        fun getEmptyModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "", "", "", false, false, Date(),
//                Date(), 0, null, false, mutableListOf()
//            )
//        }
//
//        fun getFilledModel(): ScreenOnStatsModel {
//            return ScreenOnStatsModel(
//                "xxxxx", "test", "test", false, false, Date(),
//                Date(), 0, null, false, mutableListOf(MediaModel.getEmptyModel())
//            )
//        }
    }
}