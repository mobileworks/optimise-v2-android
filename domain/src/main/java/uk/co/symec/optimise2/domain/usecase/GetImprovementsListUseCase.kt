package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.ImprovementItemModel
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.domain.model.Result

interface GetImprovementsListUseCase {
    suspend operator fun invoke(improvementType: ImprovementType): Result<List<ImprovementItemModel>>
}