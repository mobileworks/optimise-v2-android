package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import java.util.*

interface AddScreenOnUseCase {
    suspend operator fun invoke(date: Date, screenOn: Boolean, isForeground: Boolean): Result<Boolean>
}