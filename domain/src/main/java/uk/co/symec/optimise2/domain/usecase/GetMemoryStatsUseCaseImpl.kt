package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.MemoryStatsModel
import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.MemoryStatsRepository
import javax.inject.Inject

class GetMemoryStatsUseCaseImpl @Inject constructor(private val repository: MemoryStatsRepository) :
    GetMemoryStatsUseCase {
    override suspend fun invoke(): Result<MemoryStatsModel> {
        return repository.getMemoryStats()
    }
}