package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.model.TestResultModel
import uk.co.symec.optimise2.domain.repository.TestResultRepository
import javax.inject.Inject

class AddTestResultUseCaseImpl @Inject constructor(private val repository: TestResultRepository) :
    AddTestResultUseCase {
    override suspend fun invoke(result: TestResultModel): Result<Boolean> {
        return repository.addTestResult(result)
    }
}