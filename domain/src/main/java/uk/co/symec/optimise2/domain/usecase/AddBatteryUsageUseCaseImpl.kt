package uk.co.symec.optimise2.domain.usecase

import uk.co.symec.optimise2.domain.model.Result
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository
import java.util.*
import javax.inject.Inject

class AddBatteryUsageUseCaseImpl @Inject constructor(private val repository: BatteryUsageRepository) :
    AddBatteryUsageUseCase {
    override suspend fun invoke(
        date: Date,
        health: String?,
        powerSource: String?,
        status: String?,
        voltage: Float,
        temp: Float,
        batteryCapacity: Int,
        currentNow: Int,
        currentAvg: Int,
        remainingCapacity: Int
    ): Result<Boolean> {
        return repository.addBatteryUsage(
            date,
            health,
            powerSource,
            status,
            voltage,
            temp,
            batteryCapacity,
            currentNow,
            currentAvg,
            remainingCapacity
        )
    }
}