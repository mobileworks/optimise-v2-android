package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.StorageStatsRepository

class GetStorageStatsUseCaseTest {

    private val storageStatsRepository: StorageStatsRepository = mock(StorageStatsRepository::class.java)
    private val getStorageStatsUseCase by lazy { GetStorageStatsUseCaseImpl(storageStatsRepository) }

    @Test
    fun `test GetStorageStatsUseCase calls StorageStatsRepository`() {
        runBlocking {
            getStorageStatsUseCase()
            verify(storageStatsRepository).getStorageStats()
        }
    }
}