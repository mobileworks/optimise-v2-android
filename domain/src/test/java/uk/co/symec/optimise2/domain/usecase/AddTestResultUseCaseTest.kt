package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.model.TestResultModel
import uk.co.symec.optimise2.domain.repository.TestResultRepository

class AddTestResultUseCaseTest {

    private val testResultRepository: TestResultRepository = mock(TestResultRepository::class.java)
    private val addTestResultUseCase by lazy { AddTestResultUseCaseImpl(testResultRepository) }

    @Test
    fun `test AddTestResultUseCase calls TestResultRepository`() {
        runBlocking {
            val testResult = TestResultModel.getEmptyModel()
            addTestResultUseCase(testResult)
            verify(testResultRepository).addTestResult(testResult)
        }
    }
}