package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.ScreenOnRepository
import java.util.*

class GetDailyScreenOnTimeUseCaseTest {

    private val screenOnRepository: ScreenOnRepository = mock(ScreenOnRepository::class.java)
    private val getDailyScreenOnTimeUseCase by lazy { GetDailyScreenOnTimeUseCaseImpl(screenOnRepository) }

    @Test
    fun `test GetDailyScreenOnTimeUseCase calls ScreenOnRepository`() {
        runBlocking {
            val date = Date()
            getDailyScreenOnTimeUseCase(date)
            verify(screenOnRepository).getDailyScreenOnTime(date)
        }
    }
}