package uk.co.symec.optimise2.domain.utils

import java.util.*

const val REMAINING_CAPACITY = 1000
const val DEVICE_ID = "1234"
const val CURRENT_AVG = 300
const val CURRENT_NOW = 300
const val BATTERY_CAPACITY = 3000
const val TEMP = 32.4f
const val VOLTAGE = 4.12f
const val STATUS = "charging"
const val POWER_SOURCE = "name"
const val HEALTH = "good"
val DATE = Date(1625742919)

const val FAKE_FAILURE_ERROR_CODE = 400