package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.ScreenOnRepository
import uk.co.symec.optimise2.domain.utils.DATE

class AddScreenOnUseCaseTest {

    private val screenOnRepository: ScreenOnRepository = mock(ScreenOnRepository::class.java)
    private val addScreenOnUseCase by lazy { AddScreenOnUseCaseImpl(screenOnRepository) }

    @Test
    fun `test AddScreenOnUseCase calls ScreenOnRepository`() {
        runBlocking {
            addScreenOnUseCase(DATE,true, true)
            verify(screenOnRepository).addScreenOn(DATE,true, true)
        }
    }
}