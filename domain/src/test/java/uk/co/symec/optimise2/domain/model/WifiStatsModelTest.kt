package uk.co.symec.optimise2.domain.model

import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class WifiStatsModelTest {

    @Test
    fun `test getWifiPercentage return critical`() {
        runBlocking {
            val model = WifiStatsModel.getEmptyModelWithSignalStrength(-90)
            Assert.assertEquals(model.getPercentage(), 1)
        }
    }

    @Test
    fun `test getWifiPercentage return critical 2`() {
        runBlocking {
            val model = WifiStatsModel.getEmptyModelWithSignalStrength(-81)
            Assert.assertEquals(model.getPercentage(), 10)
        }
    }

    @Test
    fun `test getWifiPercentage return warning`() {
        runBlocking {
            val model = WifiStatsModel.getEmptyModelWithSignalStrength(-80)
            Assert.assertEquals(model.getPercentage(), 11)
        }
    }

    @Test
    fun `test getWifiPercentage return warning 2`() {
        runBlocking {
            val model = WifiStatsModel.getEmptyModelWithSignalStrength(-71)
            Assert.assertEquals( model.getPercentage(), 20)
        }
    }

    @Test
    fun `test getWifiPercentage return ok`() {
        runBlocking {
            val model = WifiStatsModel.getEmptyModelWithSignalStrength(-70)
            Assert.assertEquals(model.getPercentage(), 21)
        }
    }

    @Test
    fun `test getWifiPercentage return ok 2`() {
        runBlocking {
            val model = WifiStatsModel.getEmptyModelWithSignalStrength(-30)
            Assert.assertEquals(model.getPercentage(), 100)
        }
    }
}