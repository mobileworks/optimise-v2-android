package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository

class GetBatteryInfoUseCaseTest {

    private val batteryUsageRepository: BatteryUsageRepository = mock(BatteryUsageRepository::class.java)
    private val getBatteryInfoUseCase by lazy { GetBatteryInfoUseCaseImpl(batteryUsageRepository) }

    @Test
    fun `test GetBatteryInfoUseCase calls BatteryUsageRepository`() {
        runBlocking {
            getBatteryInfoUseCase()
            verify(batteryUsageRepository).getBatteryInfo()
        }
    }
}