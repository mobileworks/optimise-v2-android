package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.MemoryStatsRepository

class GetMemoryStatsUseCaseTest {

    private val memoryStatsRepository: MemoryStatsRepository = mock(MemoryStatsRepository::class.java)
    private val getMemoryStatsUseCase by lazy { GetMemoryStatsUseCaseImpl(memoryStatsRepository) }

    @Test
    fun `test GetMemoryStatsUseCase calls MemoryStatsRepository`() {
        runBlocking {
            getMemoryStatsUseCase()
            verify(memoryStatsRepository).getMemoryStats()
        }
    }
}