package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository

class GetBatteryHealthUseCaseTest {

    private val batteryUsageRepository: BatteryUsageRepository = mock(BatteryUsageRepository::class.java)
    private val getBatteryHealthUseCase by lazy { GetBatteryHealthUseCaseImpl(batteryUsageRepository) }

    @Test
    fun `test GetBatteryHealthUseCase calls BatteryUsageRepository`() {
        runBlocking {
            getBatteryHealthUseCase()
            verify(batteryUsageRepository).getBatteryHealth()
        }
    }
}