package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.TestResultRepository

class GetLastTestResultUseCaseTest {

    private val testResultRepository: TestResultRepository = mock(TestResultRepository::class.java)
    private val getLastTestResultUseCase by lazy { GetLastTestResultUseCaseImpl(testResultRepository) }

    @Test
    fun `test GetLastTestResultUseCase calls TestResultRepository`() {
        runBlocking {
            getLastTestResultUseCase()
            verify(testResultRepository).getLastTestResult()
        }
    }
}