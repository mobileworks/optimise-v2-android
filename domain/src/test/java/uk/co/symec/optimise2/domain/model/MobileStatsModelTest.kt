package uk.co.symec.optimise2.domain.model

import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class MobileStatsModelTest {

    @Test
    fun `test getGsmPercentage return critical`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-130)
            Assert.assertEquals(model.getPercentage(), 1)
        }
    }

    @Test
    fun `test getGsmPercentage return critical 2`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-121)
            Assert.assertEquals(model.getPercentage(), 10)
        }
    }

    @Test
    fun `test getGsmPercentage return warning`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-120)
            Assert.assertEquals(model.getPercentage(), 11)
        }
    }

    @Test
    fun `test getGsmPercentage return warning 2`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-106)
            Assert.assertEquals( model.getPercentage(), 20)
        }
    }

    @Test
    fun `test getGsmPercentage return ok`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-105)
            Assert.assertEquals(model.getPercentage(), 21)
        }
    }

    @Test
    fun `test getGsmPercentage return ok 2`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-30)
            Assert.assertEquals(model.getPercentage(), 100)
        }
    }

    @Test
    fun `test getGsmPercentage return ok 3`() {
        runBlocking {
            val model = MobileStatsModel.getEmptyModelWithSignalStrength(-97)
            Assert.assertEquals(model.getPercentage(), 33)
        }
    }
}