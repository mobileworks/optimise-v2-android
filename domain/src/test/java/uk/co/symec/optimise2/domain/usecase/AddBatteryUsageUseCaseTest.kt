package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.repository.BatteryUsageRepository
import uk.co.symec.optimise2.domain.utils.*

class AddBatteryUsageUseCaseTest {

    private val batteryUsageRepository: BatteryUsageRepository = mock(BatteryUsageRepository::class.java)
    private val addBatteryUsageUseCase by lazy { AddBatteryUsageUseCaseImpl(batteryUsageRepository) }

    @Test
    fun `test AddBatteryUsageUseCase calls BatteryUsageRepository`() {
        runBlocking {
            addBatteryUsageUseCase(DATE, HEALTH, POWER_SOURCE, STATUS, VOLTAGE, TEMP,
                BATTERY_CAPACITY, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY)
            verify(batteryUsageRepository).addBatteryUsage(DATE, HEALTH, POWER_SOURCE, STATUS, VOLTAGE, TEMP,
                BATTERY_CAPACITY, CURRENT_NOW, CURRENT_AVG, REMAINING_CAPACITY)
        }
    }
}