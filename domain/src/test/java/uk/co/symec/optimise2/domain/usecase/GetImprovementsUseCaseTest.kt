package uk.co.symec.optimise2.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.optimise2.domain.model.ImprovementType
import uk.co.symec.optimise2.domain.repository.ImprovementsRepository

class GetImprovementsUseCaseTest {

    private val improvementsRepository: ImprovementsRepository = mock(ImprovementsRepository::class.java)
    private val getImprovementsListUseCase by lazy { GetImprovementsListUseCaseImpl(improvementsRepository) }

    @Test
    fun `test GetImprovementsListUseCase calls ImprovementsRepository`() {
        runBlocking {
            getImprovementsListUseCase(ImprovementType.ALERT)
            verify(improvementsRepository).getImprovements(ImprovementType.ALERT)
        }
    }
}